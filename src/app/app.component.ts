import {
  Component
} from '@angular/core';
import {
  Platform,
  MenuController,
  NavController,
  LoadingController,
  AlertController,
} from '@ionic/angular';
import {
  SplashScreen
} from '@ionic-native/splash-screen/ngx';
import {
  StatusBar
} from '@ionic-native/status-bar/ngx';
// PLUGINS
import {
  Network
} from '@ionic-native/network/ngx';
import {
  Storage
} from '@ionic/storage';
import {
  Router,
  NavigationExtras
} from '@angular/router';
import firebase from 'firebase';

var firebaseConfig = {
  apiKey: "AIzaSyBIcvqFdyFceWOdjNnejgBd19siDpTbjPo",
  authDomain: "moppa-system.firebaseapp.com",
  databaseURL: "https://moppa-system.firebaseio.com",
  projectId: "moppa-system",
  storageBucket: "moppa-system.appspot.com",
  messagingSenderId: "890358165682",
  appId: "1:890358165682:web:9204b941b2b41d702459ab",
  measurementId: "G-792DW2CQ9C"
};

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  rol: any;

  constructor(
    public menu: MenuController,
    public navCtrl: NavController,
    public loadingController: LoadingController,
    private network: Network,
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private storage: Storage,
    private router: Router,
    private alertCtrl: AlertController,
  ) {
    this.initializeApp();
    // Initialize Firebase
    firebase.initializeApp(firebaseConfig);
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      this.handleGetStorageUser();
    });
  }

  async handleGetStorageUser() {
    this.storage.get("user").then(
      user => {
        this.rol = user.userInfo.roleType;
        console.log("este es el rol", this.rol)

        if (this.rol === 3) {
          this.router.navigate(['home']);
        } else if (this.rol === 4) {
          this.router.navigate(['/techome']);
        } else {
          this.router.navigateByUrl('login');
        }
      }
    )
  }

}