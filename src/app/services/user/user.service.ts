import {
  Observable,
  throwError
} from 'rxjs';

import {
  HttpClient,
  HttpHeaders,
  HttpErrorResponse
} from '@angular/common/http';

import {
  catchError,
  map
} from 'rxjs/operators';

import {
  Injectable
} from '@angular/core';

import {
  Storage
} from '@ionic/storage';

import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';


@Injectable({
  providedIn: 'root'
})
export class UserService {
  chatsServices: AngularFirestoreCollection;
  ;

  constructor(
    public http: HttpClient,
    private storage: Storage,
    private afStore: AngularFirestore
  ) {
    this.chatsServices = this.afStore.collection('chats_services');
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error('An error has been occurred:', error.error.message);
    } else {
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    return throwError('Something bad happened; please try again later.');
  }

  private extractData(res: Response) {
    let body = res;
    return body || {};
  }

  getChat(uid: string){
    const query = this.chatsServices.ref.doc(uid);
    return query.get();
  }

  updateChanges(uid: string){
    const query = this.chatsServices.ref.doc(uid);
    return query;
  }

  updateChat(uid:string, chat: any){
    return this.chatsServices.doc(uid).update(chat);
  }

  handleGetServiceOffline(data) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InNlY29uZE5hbWUiOiJBZG1pbiBBZG1pbiIsImJpcnRoZGF0ZSI6eyJfc2Vjb25kcyI6MTYwNjYyOTYwMCwiX25hbm9zZWNvbmRzIjowfSwiY3JlYXRlQnkiOiJQZk52U1VRa3FDU1dkOFRtenhlMnV3ZlJIaHkyIiwiZW1haWwiOiJhZG1pbkBtb3BwYS5jb20iLCJ0eXBlUmVnaXN0ZXIiOjEsInJvbGVUeXBlIjoxLCJwaG9uZSI6IjU4Mzg3MzIzIiwidWlkIjoiUGZOdlNVUWtxQ1NXZDhUbXp4ZTJ1d2ZSSGh5MiIsImZpcnN0TmFtZSI6IkFkbWluIEFkbWluIiwicm9sZU5hbWUiOiJBZG1pbiIsImdlbmRlclR5cGUiOjEsImNyZWF0ZURhdGUiOnsiX3NlY29uZHMiOjE2MDYwMjQ4MDAsIl9uYW5vc2Vjb25kcyI6MH0sInVwZGF0ZURhdGUiOnsiX3NlY29uZHMiOjE2MDYwMjQ4MDAsIl9uYW5vc2Vjb25kcyI6MH0sImdlbmRlciI6Ik1hc2N1bGlubyIsInN0YXR1cyI6MSwidXBkYXRlQnkiOiJQZk52U1VRa3FDU1dkOFRtenhlMnV3ZlJIaHkyIn0sImlhdCI6MTYxMTE5OTA4OSwiZXhwIjoxNjEzNzkxMDg5fQ.vzi6h1wnMd7sMBvazzQO-9OETtYEcFaDlDqH0AmfEh0'
      })
    }
    let url = 'https://moppa-system.uc.r.appspot.com/services/filter'; 
    return this.http.post(url, data, httpOptions)
  }

  handleGetServiceOnline(data, token) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': token
      })
    }
    let url = 'https://moppa-system.uc.r.appspot.com/services/filter'; 
    return this.http.post(url, data, httpOptions)
  }

  handleGetTecnicos(data, token) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': token
      })
    }
    let url = 'https://moppa-system.uc.r.appspot.com/users/filter'; 
    return this.http.post(url, data, httpOptions)
  }

  handlePostLocation(data, token) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': token
      })
    }
    let url = 'https://moppa-system.uc.r.appspot.com/users/locations'; 
    return this.http.post(url, data, httpOptions)
  }

  handlePostService(data, token) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': token
      })
    }
    let url = 'https://moppa-system.uc.r.appspot.com/services/request/'; 
    return this.http.post(url, data, httpOptions)
  }

  handleGetServicioss(data, token) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': token
      })
    }
    let url = 'https://moppa-system.uc.r.appspot.com/services/request/filter'; 
    return this.http.post(url, data, httpOptions)
  }

  handlePutService(data, token, request) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': token
      })
    }
    let url = 'https://moppa-system.uc.r.appspot.com/services/request/' + request;
    console.log(url); 
    return this.http.put(url, data, httpOptions)
  }

  handlePutService2(data, token, request) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': token
      })
    }
    let url = 'https://moppa-system.uc.r.appspot.com/services/request/' + request;
    console.log(url); 
    return this.http.put(url, data, httpOptions)
  }

  handleSendToken(data, token) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': token
      })
    }
    let url = 'https://moppa-system.uc.r.appspot.com/notification/add'; 
    return this.http.post(url, data, httpOptions)
  }

  handleSendNotification(data, token) {
    console.log(data)
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': token
      })
    }
    let url = 'https://moppa-system.uc.r.appspot.com/notification'; 
    return this.http.post(url, data, httpOptions)
  }

  handlecheckService(data, token) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': token
      })
    }
    let url = 'https://moppa-system.uc.r.appspot.com/services/request/filter'; 
    return this.http.post(url, data, httpOptions)
  }

  // PAGALO 

  handlePay(data){
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    }
    let url = 'https://app.pagalocard.com/api/v1/integracionpg/e3q66LIT1WJPbDaf4Kzo'; 
    return this.http.post(url, data, httpOptions)
  }
}
