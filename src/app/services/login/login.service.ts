import {
  Observable,
  throwError
} from 'rxjs';

import {
  HttpClient,
  HttpHeaders,
  HttpErrorResponse
} from '@angular/common/http';

import {
  catchError,
  map
} from 'rxjs/operators';

import {
  Injectable
} from '@angular/core';

import {
  Storage
} from '@ionic/storage';

import {
  Subject,
  Subscription
} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  private fooSubject = new Subject < any > ();

  constructor(public http: HttpClient,
    private storage: Storage
  ) {}

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error('An error has been occurred:', error.error.message);
    } else {
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    return throwError('Something bad happened; please try again later.');
  }

  private extractData(res: Response) {
    let body = res;
    return body || {};
  }

  handleLog(data) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    }

    let url = 'https://moppa-system.uc.r.appspot.com/login'; 
    return this.http.post(url, data, httpOptions)
  }

  publishSomeData(data: any) {
    this.fooSubject.next(data);
  }

  getObservable(): Subject < any > {
    return this.fooSubject;
  }

  handleRegister(data) {
    console.log(data)
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    }

    let url = 'https://moppa-system.uc.r.appspot.com/users'; 
    return this.http.post(url, data, httpOptions)
  }

}
