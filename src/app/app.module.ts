import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Network } from '@ionic-native/network/ngx';
import { IonicStorageModule } from '@ionic/storage';
import { HttpClientModule, HttpClient, HttpClientJsonpModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { NativeGeocoder } from '@ionic-native/native-geocoder/ngx';

import { SharedComponentsModule } from 'src/app/components/sharedcomponent/sharedcomponent.module';
import { FCM } from '@ionic-native/fcm/ngx';
import {
  LaunchNavigator,
  LaunchNavigatorOptions
} from '@ionic-native/launch-navigator/ngx';
import { environment } from 'src/environments/environment';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { EmailComposer } from '@ionic-native/email-composer/ngx';

import { AngularFireModule } from "@angular/fire";
import { AngularFireStorageModule, BUCKET } from '@angular/fire/storage';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireAuthModule } from '@angular/fire/auth';

import { ReactiveFormsModule } from '@angular/forms';
import { Facebook } from '@ionic-native/facebook/ngx';
import { GooglePlus } from '@ionic-native/google-plus/ngx';

// IMPORT APPLE SIGN IN
import { SignInWithApple } from "@ionic-native/sign-in-with-apple/ngx";
import { ImagePicker } from '@ionic-native/image-picker/ngx';
import { File } from '@ionic-native/file/ngx';
import { PhotoLibrary } from '@ionic-native/photo-library/ngx';

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [BrowserModule, IonicModule.forRoot(), 
    ReactiveFormsModule,
    AppRoutingModule,
    IonicStorageModule.forRoot(),
    HttpClientModule,
    HttpClientJsonpModule,
    SharedComponentsModule,
    AngularFireStorageModule,
    AngularFirestoreModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireAuthModule
  ],
  providers: [
    { provide: BUCKET, useValue: 'moppa-system.appspot.com' },
    EmailComposer,
    NativeGeocoder,
    Network,
    GooglePlus,
    FCM,
    SignInWithApple,
    Camera,
    PhotoLibrary,
    LaunchNavigator,
    Geolocation,
    StatusBar,
    Facebook,
    SplashScreen,
    ImagePicker,
    File
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
