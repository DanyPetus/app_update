import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { SidemenuusuComponent } from '../sidemenuusu/sidemenuusu.component'
import { SidemenutecComponent } from '../sidemenutec/sidemenutec.component'


@NgModule({
  declarations: [SidemenuusuComponent, SidemenutecComponent],
  imports: [ 
    FormsModule,  
    CommonModule,
    IonicModule
  ],
  exports: [SidemenuusuComponent, SidemenutecComponent]
})
export class SharedComponentsModule { }