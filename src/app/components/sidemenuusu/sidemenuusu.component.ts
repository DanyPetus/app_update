import { Component, OnInit } from '@angular/core';
import {
  Storage
} from '@ionic/storage';
import {
  Router,
  NavigationExtras
} from '@angular/router';
// SERVICES
import {
  LoginService
} from '../../services/login/login.service';
import {
  Facebook,
  FacebookLoginResponse
} from '@ionic-native/facebook/ngx';
import { GooglePlus } from '@ionic-native/google-plus/ngx';

@Component({
  selector: 'app-sidemenuusu',
  templateUrl: './sidemenuusu.component.html',
  styleUrls: ['./sidemenuusu.component.scss'],
})
export class SidemenuusuComponent implements OnInit {
  nombre: string;
  apellido : string;
  nombreCompleto : string;

  rol: string;
  codigo: number;
  photo: string;

  isLoggedIn = false;
  users = {
    id: '',
    name: '',
    email: '',
    picture: {
      data: {
        url: ''
      }
    }
  };

  constructor(
    private storage: Storage,
    public router: Router,
    private _loginService: LoginService,
    private fb: Facebook,
    private googlePlus: GooglePlus,
  ) {     
    fb.getLoginStatus()
    .then(res => {
      console.log(res.status);
      if (res.status === 'connect') {
        this.isLoggedIn = true;
      } else {
        this.isLoggedIn = false;
      }
    })
    .catch(e => console.log(e));
  }

  ionViewDidEnter	() {
  }

  ngOnInit() {
    this._loginService.getObservable().subscribe((data) => {
      console.log('Data received', data);
      this.nombre = data.foo.userInfo.firstName;
      this.apellido = data.foo.userInfo.secondName;
      this.nombreCompleto = this.nombre + ' ' + this.apellido;
      this.rol = data.foo.userInfo.roleName;
    });

    this.handleGetStorageUser();
  }

  goSalir() {
    if (this.isLoggedIn === true){
      this.storage.clear();
      this.router.navigateByUrl('intro');
      this.logout();
    } else {
      this.storage.clear();
      this.router.navigateByUrl('intro');
    }
  }

  logout () { 
    this.fb.logout () 
      .then (res => this.isLoggedIn = false) 
      .catch (e => console.log ('Error al cerrar sesión en Facebook', e)); 
  }

  // logoutGgl(){
  //   this.googlePlus.logout({})
  //   .then((result:any) => {
  //     // this.userData = result;
  //   })
  //   .catch(err => {

  //   }) ;
  // }

  async handleGetStorageUser() {
    this.storage.get("user").then(
      user => {
        this.nombre = user.userInfo.firstName;
        this.apellido = user.userInfo.secondName;
        this.nombreCompleto = this.nombre + ' ' + this.apellido;
        this.rol = user.userInfo.roleName;
      })
  };

  goPedido(){
    this.router.navigateByUrl('historial');
  }

  goHerramientas(){
    this.router.navigateByUrl('security');
  }

}
