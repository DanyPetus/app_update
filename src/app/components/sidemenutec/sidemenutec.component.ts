import { Component, OnInit } from '@angular/core';
import {
  Storage
} from '@ionic/storage';
import {
  Router,
  NavigationExtras
} from '@angular/router';
// SERVICES
import {
  LoginService
} from '../../services/login/login.service';

@Component({
  selector: 'app-sidemenutec',
  templateUrl: './sidemenutec.component.html',
  styleUrls: ['./sidemenutec.component.scss'],
})
export class SidemenutecComponent implements OnInit {
  nombre: string;
  apellido : string;
  nombreCompleto : string;

  rol: string;
  codigo: number;
  photo: string;

  constructor(
    private storage: Storage,
    public router: Router,
    private _loginService: LoginService,
  ) { }

  ngOnInit() {
    this._loginService.getObservable().subscribe((data) => {
      console.log('Data received', data);
      this.nombre = data.foo.userInfo.firstName;
      this.apellido = data.foo.userInfo.secondName;
      this.nombreCompleto = this.nombre + ' ' + this.apellido;
      this.rol = data.foo.userInfo.roleName;
    });

    this.handleGetStorageUser();
  }

  goSalir() {
    this.storage.clear();
    this.router.navigateByUrl('intro');
  }

  async handleGetStorageUser() {
    this.storage.get("user").then(
      user => {
        this.nombre = user.userInfo.firstName;
        this.apellido = user.userInfo.secondName;
        this.nombreCompleto = this.nombre + ' ' + this.apellido;
        this.rol = user.userInfo.roleName;
      })
  };

  goPedido(){
    this.router.navigateByUrl('pedidostec');
  }

}
