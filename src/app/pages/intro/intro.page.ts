import {
  Component,
  OnInit,
  ViewChild
} from '@angular/core';
import {
  LoadingController,
  AlertController,
  NavController,
  Platform,
  IonSlides,
  MenuController,
} from '@ionic/angular';
import {
  Router,
  ActivatedRoute,
  ParamMap
} from "@angular/router";
import {
  Facebook,
  FacebookLoginResponse
} from '@ionic-native/facebook/ngx';
import {
  LoginService
} from '../../services/login/login.service';
import {
  Network
} from '@ionic-native/network/ngx';
import {
  Storage
} from '@ionic/storage';
import {
  AngularFireAuth
} from '@angular/fire/auth';
import firebase from 'firebase/app';
import {
  GooglePlus
} from '@ionic-native/google-plus/ngx';
import {
  SignInWithApple,
  ASAuthorizationAppleIDRequest,
  AppleSignInResponse,
  AppleSignInErrorResponse
} from "@ionic-native/sign-in-with-apple/ngx";

@Component({
  selector: 'app-intro',
  templateUrl: './intro.page.html',
  styleUrls: ['./intro.page.scss'],
})
export class IntroPage implements OnInit {

  @ViewChild('slidess', {
    static: true
  }) slides: IonSlides;
  loading: any;
  animate: any;
  typeNetwork: string;

  isLoggedIn = false;
  users = {
    id: '',
    name: '',
    email: '',
    picture: {
      data: {
        url: ''
      }
    }
  };
  email: any;
  password: any;
  nombre: any;
  apellido: any;
  registro: any;
  login: any;

  pictureGgl;
  nameGgl;
  emailGgl;

  userData: any = {};
  seeIOS : boolean;

  statusandroid : any;
  statusapple : any;

  constructor(
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController,
    private router: Router,
    public navCtrl: NavController,
    private menu: MenuController,
    private storage: Storage,
    private googlePlus: GooglePlus,
    private fb: Facebook,
    private _loginProvider: LoginService,
    private network: Network,
    public loadingController: LoadingController,
    private afAuth: AngularFireAuth,
    public platform: Platform,
    private signInWithApple: SignInWithApple
  ) {
    fb.getLoginStatus()
      .then(res => {
        console.log(res.status);
        if (res.status === 'connect') {
          this.isLoggedIn = true;
        } else {
          this.isLoggedIn = false;
        }
      })
      .catch(e => console.log(e));

      if (this.platform.is("ios")){
        this.seeIOS = true;
      } else {
        this.seeIOS = false;
      }

      this.menu.enable(false, 'first');
      this.menu.enable(false, 'second');
  }


  ngOnInit() {
    this.openFirst();
    this.dataApple();
  }

  fbLogin() {
    this.fb.login(['public_profile', 'email'])
      .then(res => {
        if (res.status === 'connected') {
          this.isLoggedIn = true;
          this.getUserDetail(res.authResponse.userID);
        } else {
          this.isLoggedIn = false;
        }
      })
      .catch(e => console.log('Error logging into Facebook', e));
  }

  getUserDetail(userid: any) {
    this.fb.api('/' + userid + '/?fields=id,email,name,picture', ['public_profile'])
      .then(res => {
        console.log(res);
        this.users = res;
        this.email = this.users.email;
        this.nombre = this.users.name;
        this.goRegister();
      })
      .catch(e => {
        console.log(e);
      });
  }

  async goRegister() {
    this.typeNetwork = this.network.type;
    if (this.typeNetwork != 'none') {
      let data = {
        "user": {
          "email": this.email,
          "password": "12345678",
        },
        "information": {
          "firstName": this.nombre,
          "secondName": "",
          "phone": "12345678",
          "birthdate": new Date(),
          "gender": "Sin Especificar",
          "genderType": 3,
          "roleName": "Cliente",
          "roleType": 3,
          "imageProfile": "",
          "status": 1,
          "typeRegister": 1,
          "createBy": "PfNvSUQkqCSWd8Tmzxe2uwfRHhy2",
          "updateBy": "PfNvSUQkqCSWd8Tmzxe2uwfRHhy2"
        }
      }
      console.log(data);
      await this._loginProvider.handleRegister(data)
        .subscribe((res) => {
          this.registro = res;
          this.goToHome();
        }, (err) => {
          this.goToHome();
          console.error(err);
        });
    } else {
      this.alertMsj(
        "Error de conexión",
        "Verifica tu conexión a internet."
      );
    }
  }

  async goToHome() {
    this.typeNetwork = this.network.type;
    if (this.typeNetwork != 'none') {
      let data = {
        'email': this.email,
        'password': "12345678",
      };
      console.log(data);
      const loading = await this.loadingController.create({
        message: 'Cargando...',
        duration: 3000
      });
      await loading.present();
      await this._loginProvider.handleLog(data)
        .subscribe((res) => {
          this.login = res;
          this.storage.set('user', this.login);
          this.storage.set('status', 1);
          this.router.navigate(['/home']);
          loading.dismiss();

        }, (err) => {
          loading.dismiss();
          console.error(err.status);
        });
    } else {
      this.alertMsj(
        "Error de conexión",
        "Verifica tu conexión a internet."
      );
    }
  }

  async alertMsj(header: string, message: string) {
    const alert = await this.alertCtrl.create({
      header: header,
      message: message,
      buttons: ['Ok'],
      mode: 'ios'
    });
    alert.present();
  }

  logout() {
    this.fb.logout()
      .then(res => this.isLoggedIn = false)
      .catch(e => console.log('Error al cerrar sesión en Facebook', e));
  }

  async loginGoogleAndroid() {
    this.googlePlus.login({})
      .then((result: any) => {
        this.userData = result;
        this.email = this.userData.email;
        this.nombre = this.userData.displayName;
        this.goRegister();
        console.log(this.userData)
      })
      .catch(err => {
        this.alertMsj(
          `Error ${JSON.stringify(err)}`,
          "Por favor, intente de nuevo..."
        );
      });
  }

  async loginGoogleWeb() {
    const res = await this.afAuth.signInWithPopup(new firebase.auth.GoogleAuthProvider());
    const user = res.user;
    console.log(user);
    this.pictureGgl = user.photoURL;
    this.nameGgl = user.displayName;
    this.emailGgl = user.email;
  }

  AppleSignIn() {
    this.signInWithApple
      .signin({
        requestedScopes: [
          ASAuthorizationAppleIDRequest.ASAuthorizationScopeFullName,
          ASAuthorizationAppleIDRequest.ASAuthorizationScopeEmail
        ]
      })
      .then((res: AppleSignInResponse) => {
        console.log("Apple login success:- " + res);
        this.email = res.email;
        this.nombre = res.fullName.givenName + " " + res.fullName.familyName;
        this.goRegister();
        console.log(res.fullName);
        console.log(res.email);
      })
      .catch((error: AppleSignInErrorResponse) => {
        console.error(error);
      });
  }

  openFirst() {
    this.menu.enable(false, 'first');
  }

  onSlideChange() {
    this.slides.getActiveIndex()
      .then(index => {
        console.log(index);
        if (index != 0) {
          this.switchAnimates()
        }
        if (index == 3) {
          this.animate = 'animated swing'
        }
      })
  }

  switchAnimates() {
    this.animate = "animated pulse"
    console.log(this.animate + "beg");
    setTimeout(() => {
      this.animate = "";
      console.log(this.animate + "over");
    }, 2000);
  }

  goToRegister() {
    this.router.navigate(['/registro']);
  }

  goToLogin() {
    this.router.navigate(['/login']);
  }

  goServices() {
    this.router.navigate(['/home']);
    this.storage.set('status', 0);
  }

  dataApple(){
    		// console.log(id);
		let messagesRef = firebase.database().ref("apple").child('0');
		messagesRef.on("value", (snap) => {
			let data = snap.val();
			console.log(data);
      this.statusandroid = data.statusandroid;
      this.statusapple = data.statusapple;
		});
  }

}