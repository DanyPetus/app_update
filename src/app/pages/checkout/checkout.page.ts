import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  NgZone
} from '@angular/core';
import {
  NativeGeocoder,
  NativeGeocoderResult,
  NativeGeocoderOptions
} from '@ionic-native/native-geocoder/ngx';
import {
  NavController,
  Platform,
  AlertController,
  LoadingController,
  MenuController,
  ModalController
} from '@ionic/angular';
import {
  ModalOrderPage
} from '../modal-order/modal-order.page';
import {
  Geolocation
} from '@ionic-native/geolocation/ngx';

import {
  ActivatedRoute,
  Router,
  NavigationExtras
} from '@angular/router';
// PLUGINS
import {
  Network
} from '@ionic-native/network/ngx';
import {
  Storage
} from '@ionic/storage';
import {
  UserService
} from '../../services/user/user.service';

declare var google;


@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.page.html',
  styleUrls: ['./checkout.page.scss'],
})
export class CheckoutPage implements OnInit {
  @ViewChild('map', {
    static: false
  }) mapElement: ElementRef;
  map: any;
  address: string;
  lat: string;
  long: string;
  autocomplete: {
    input: string;
  };
  autocompleteItems: any[];
  location: any;
  placeid: any;
  GoogleAutocomplete: any;

  latitud: any;
  longitud: any;

  data: any;
  locaciones: any;
  typeNetwork: string;
  token: string;
  uid: string;
  nameLocation: string = "";
  noteLocation: string = "";

  placesService: any;

  constructor(private navCtrl: NavController,
    private router: Router,
    private platform: Platform,
    private modalCtrl: ModalController,
    public loadingController: LoadingController,
    private geolocation: Geolocation,
    private route: ActivatedRoute,
    private network: Network,
    private storage: Storage,
    private alertCtrl: AlertController,
    private _userProvider: UserService,
    private nativeGeocoder: NativeGeocoder,
    public zone: NgZone,
  ) {
    this.GoogleAutocomplete = new google.maps.places.AutocompleteService();
    this.autocomplete = {
      input: ''
    };
    this.autocompleteItems = [];
  }

  ngOnInit() {
    this.presentRememberGPS();
    // Route recibe los parametros (se debe importar)
    this.route.queryParams.subscribe(params => {
      // Valida que exista params y params.dataCard que son los datos que recibiremos
      if (params && params.dataCard) {
        // Los parseamos nuevamente a objeto y podemos acceder a sus propiedades
        this.data = JSON.parse(params.dataCard);
        console.log(this.data);
      }
    });
    this.loadMap();
  }

  ionViewWillLeave() {
    this.map.remove();
  }

  //CARGAR EL MAPA TIENE DOS PARTES 
  loadMap() {

    //OBTENEMOS LAS COORDENADAS DESDE EL TELEFONO.
    this.geolocation.getCurrentPosition().then((resp) => {
      let latLng = new google.maps.LatLng(resp.coords.latitude, resp.coords.longitude);
      let mapOptions = {
        center: latLng,
        zoom: 17,
        styles: [{
            elementType: 'geometry',
            stylers: [{
              color: '#242f3e'
            }]
          },
          {
            elementType: 'labels.text.stroke',
            stylers: [{
              color: '#242f3e'
            }]
          },
          {
            elementType: 'labels.text.fill',
            stylers: [{
              color: '#746855'
            }]
          },
          {
            featureType: 'administrative.locality',
            elementType: 'labels.text.fill',
            stylers: [{
              color: '#d59563'
            }]
          },
          {
            featureType: 'poi',
            elementType: 'labels.text.fill',
            stylers: [{
              color: '#d59563'
            }]
          },
          {
            featureType: 'poi.park',
            elementType: 'geometry',
            stylers: [{
              color: '#263c3f'
            }]
          },
          {
            featureType: 'poi.park',
            elementType: 'labels.text.fill',
            stylers: [{
              color: '#6b9a76'
            }]
          },
          {
            featureType: 'road',
            elementType: 'geometry',
            stylers: [{
              color: '#38414e'
            }]
          },
          {
            featureType: 'road',
            elementType: 'geometry.stroke',
            stylers: [{
              color: '#212a37'
            }]
          },
          {
            featureType: 'road',
            elementType: 'labels.text.fill',
            stylers: [{
              color: '#9ca5b3'
            }]
          },
          {
            featureType: 'road.highway',
            elementType: 'geometry',
            stylers: [{
              color: '#746855'
            }]
          },
          {
            featureType: 'road.highway',
            elementType: 'geometry.stroke',
            stylers: [{
              color: '#1f2835'
            }]
          },
          {
            featureType: 'road.highway',
            elementType: 'labels.text.fill',
            stylers: [{
              color: '#f3d19c'
            }]
          },
          {
            featureType: 'transit',
            elementType: 'geometry',
            stylers: [{
              color: '#2f3948'
            }]
          },
          {
            featureType: 'transit.station',
            elementType: 'labels.text.fill',
            stylers: [{
              color: '#d59563'
            }]
          },
          {
            featureType: 'water',
            elementType: 'geometry',
            stylers: [{
              color: '#17263c'
            }]
          },
          {
            featureType: 'water',
            elementType: 'labels.text.fill',
            stylers: [{
              color: '#515c6d'
            }]
          },
          {
            featureType: 'water',
            elementType: 'labels.text.stroke',
            stylers: [{
              color: '#17263c'
            }]
          }
        ],
        mapTypeId: google.maps.MapTypeId.ROADMAP
      }

      //CUANDO TENEMOS LAS COORDENADAS SIMPLEMENTE NECESITAMOS PASAR AL MAPA DE GOOGLE TODOS LOS PARAMETROS.
      this.getAddressFromCoords(resp.coords.latitude, resp.coords.longitude);
      this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
      this.map.addListener('tilesloaded', () => {
        console.log('accuracy', this.map, this.map.center.lat());
        this.getAddressFromCoords(this.map.center.lat(), this.map.center.lng())
        this.lat = this.map.center.lat()
        console.log(this.lat);
        this.long = this.map.center.lng()
        console.log(this.long);
        this.placesService = new google.maps.places.PlacesService(this.map);
      });
    }).catch((error) => {
      console.log('Error getting location', error);
    });
  }

  getAddressFromCoords(lattitude, longitude) {
    console.log("getAddressFromCoords " + lattitude + " " + longitude);

    let options: NativeGeocoderOptions = {
      useLocale: true,
      maxResults: 5
    };
    this.nativeGeocoder.reverseGeocode(lattitude, longitude, options)
      .then((result: NativeGeocoderResult[]) => {
        this.address = "";
        let responseAddress = [];
        for (let [key, value] of Object.entries(result[0])) {
          if (value.length > 0)
            responseAddress.push(value);
        }
        responseAddress.reverse();
        for (let value of responseAddress) {
          this.address += value + ", ";
        }
        this.address = this.address.slice(0, -2);
      })
      .catch((error: any) => {
        this.address = "Address Not Available!";
      });
  }

  //FUNCION DEL BOTON INFERIOR PARA QUE NOS DIGA LAS COORDENADAS DEL LUGAR EN EL QUE POSICIONAMOS EL PIN.
  ShowCords() {
    alert('lat' + this.lat + ', long' + this.long)
  }

  //AUTOCOMPLETE, SIMPLEMENTE ACTUALIZAMOS LA LISTA CON CADA EVENTO DE ION CHANGE EN LA VISTA.
  UpdateSearchResults() {
    if (this.autocomplete.input == '') {
      this.autocompleteItems = [];
      return;
    }
    this.GoogleAutocomplete.getPlacePredictions({
        input: this.autocomplete.input
      },
      (predictions, status) => {
        this.autocompleteItems = [];
        this.zone.run(() => {
          predictions.forEach((prediction) => {

            this.autocompleteItems.push(prediction);
          });
        });
      });
  }

  //FUNCION QUE LLAMAMOS DESDE EL ITEM DE LA LISTA.
  SelectSearchResult(item) {
    this.autocompleteItems = [];

    let location = {
      lat: null,
      lng: null,
      name: item.name
    };

    this.placesService.getDetails({
      placeId: item.place_id
    }, (details) => {
      console.log(details);
      this.zone.run(() => {

        location.name = details.name;
        location.lat = details.geometry.location.lat();
        location.lng = details.geometry.location.lng();

        this.map.setCenter({
          lat: location.lat,
          lng: location.lng
        });

        this.location = location;

      });
    });


    console.log(item)
    this.noteLocation = item.description;
    console.log(item.place_id)
    this.placeid = item.place_id
    console.log(this.nameLocation)
  }


  //LLAMAMOS A ESTA FUNCION PARA LIMPIAR LA LISTA CUANDO PULSAMOS IONCLEAR.
  ClearAutocomplete() {
    this.autocompleteItems = []
    this.autocomplete.input = ''
  }

  //EJEMPLO PARA IR A UN LUGAR DESDE UN LINK EXTERNO, ABRIR GOOGLE MAPS PARA DIRECCIONES. 
  GoTo() {
    return window.location.href = 'https : //www.waze.com/ul?ll=40.75889500%2C-73.98513100&navigate=yes&zoom=17';

    // return window.location.href = 'https://www.google.com/maps/search/?api=1&query=Google&query_place_id=' + this.placeid;
  }

  async handleGetStorageUser() {
    this.storage.get("user").then(
      user => {
        this.token = user.userToken;
        this.uid = user.userInfo.uid;

        if (user) {
          this.getServices();
        } else {
          console.log('error faltan campos de el local storage')
        }
      }
    )
  }

  async getServices() {
    this.typeNetwork = this.network.type;
    if (this.nameLocation === "") {
      // this.nameLocation = "No hay datos registrados"
      this.alertMsj(
        "Dirección vacía",
        "Favor colocar la dirección para que el técnico pueda llegar sin problema"
      );
    } else {
      if (this.typeNetwork != 'none') {
        let data = {
          "uid_user": this.uid,
          "name": this.nameLocation + " - " + this.autocomplete.input,
          "latitude": this.lat,
          "length": this.long,
          "commentary": this.noteLocation
        };
        let token = this.token
        console.log(data, token);
        const loading = await this.loadingController.create({
          message: 'Cargando...',
          duration: 3000
        });
        await loading.present();
        await this._userProvider.handlePostLocation(data, token)
          .subscribe((res) => {
            this.locaciones = res;
            console.log(this.locaciones);
            // Data que enviaremos
            let data = {
              "name": this.nameLocation,
              "service": this.data.service,
              "location": this.locaciones,
              "total1": this.data.total1,
              "total": this.data.total,
              "cargos": this.data.cargos,
              "transporte": this.data.transporte,
              "rooms": this.data.rooms,
              "baños": this.data.baños,
              "roomsPrice": this.data.roomsPrice,
              "bañosPrice": this.data.bañosPrice
            }
            let navigationExtras: NavigationExtras = {
              queryParams: {
                // dataCard: Nombre que indicamos que sera recibido en formularioinformacion
                // JSON.stringify(data): Recibe data y la convierte en un JSON
                dataCard: JSON.stringify(data)
              }
            };
            // Ruta de Page, NavigationExtras: Data a enviar
            this.router.navigate(['pago'], navigationExtras);
            loading.dismiss();

          }, (err) => {
            this.alertMsj(
              "Ups! Ha ocurrido un error inesperado",
              "Por favor, intente de nuevo..."
            );
            loading.dismiss();
            console.error(err);
          });
      } else {
        this.alertMsj(
          "Error de conexión",
          "Verifica tu conexión a internet."
        );
      }
    }
  }

  goBack() {
    this.router.navigate(['/calculos']);
  }

  async presentAlertConfirm() {
    const alert = await this.alertCtrl.create({
      mode: "ios",
      header: '¿Desea salir de la pantalla?',
      message: '¿Seguro que desea salir?',
      buttons: [{
        text: 'Cancelar',
        role: 'cancel',
        cssClass: 'secondary',
        handler: (blah) => {

        }
      }, {
        text: 'Salir',
        handler: () => {
          this.goBack();
        }
      }]
    });

    await alert.present();
  }

  async presentRememberGPS() {
    const alert = await this.alertCtrl.create({
      mode: "ios",
      header: 'Activar GPS',
      message: 'Recuerde tener activo su sistema de GPS',
      buttons: [{
        text: 'OK',
        role: 'cancel',
        cssClass: 'secondary',
        handler: (blah) => {

        }
      }]
    });

    await alert.present();
  }

  async alertMsj(header: string, message: string) {
    const alert = await this.alertCtrl.create({
      header: header,
      message: message,
      buttons: ['Ok'],
      mode: 'ios'
    });
    alert.present();
  }

}