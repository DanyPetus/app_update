import {
  Component,
  OnInit
} from '@angular/core';
import {
  NavController,
  Platform,
  AlertController,
  LoadingController,
  MenuController,
  ModalController
} from '@ionic/angular';
import {
  Network
} from '@ionic-native/network/ngx';
import {
  Storage
} from '@ionic/storage';
import {
  ActivatedRoute,
  Router,
  NavigationExtras
} from '@angular/router';
import {
  UserService
} from '../../services/user/user.service';
import {
  ModalOrderPage
} from '../modal-order/modal-order.page';

@Component({
  selector: 'app-tecnicos',
  templateUrl: './tecnicos.page.html',
  styleUrls: ['./tecnicos.page.scss'],
})
export class TecnicosPage implements OnInit {
  typeNetwork: string;
  tecnicos:  any;
  token : any;

  arrTec : any = [];

  constructor(
    private modalCtrl: ModalController,
    private navCtrl: NavController, private router: Router,
    private platform: Platform,
    private network: Network,
    private alertCtrl: AlertController,
    private _userProvider: UserService,
    private storage: Storage,
    public loadingController: LoadingController,
    private menu: MenuController
  ) {}

  ngOnInit() {
    this.handleGetStorageUser();
  }

  async handleGetStorageUser() {
    this.storage.get("user").then(
      user => {
        this.token = user.userToken;

        if (user) {
          this.getTecnicos();
        } else {
          console.log('error faltan campos de el local storage')
        }
      }
    )
  }

  async getTecnicos() {
    this.typeNetwork = this.network.type;
    if (this.typeNetwork != 'none') {
      let data = {
        "roleType": [4],
        "status": 1
      };
      let token = this.token
      console.log(data);
      const loading = await this.loadingController.create({
        message: 'Cargando...',
        duration: 3000
      });
      await loading.present();
      await this._userProvider.handleGetTecnicos(data, token)
        .subscribe((res) => {
          this.tecnicos = res;
          console.log(this.tecnicos);
          this.arrTec = this.tecnicos.slice(0,3)
          console.log(this.arrTec);

          loading.dismiss();

        }, (err) => {
          this.alertMsj(
            "Ups! Ha ocurrido un error inesperado",
            "Por favor, intente de nuevo..."
          );
          loading.dismiss();
          console.error(err);
        });
    } else {
      this.alertMsj(
        "Error de conexión",
        "Verifica tu conexión a internet."
      );
    }
  }

  async alertMsj(header: string, message: string) {
    const alert = await this.alertCtrl.create({
      header: header,
      message: message,
      buttons: ['Ok'],
      mode: 'ios'
    });
    alert.present();
  }

}