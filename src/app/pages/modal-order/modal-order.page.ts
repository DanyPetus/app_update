import {
  Component,
  OnInit,
  Input
} from '@angular/core';
import {
  ActivatedRoute,
  Router,
  NavigationExtras
} from '@angular/router';
import {
  NavController,
  Platform,
  AlertController,
  LoadingController,
  MenuController,
  ModalController
} from '@ionic/angular';
import {
  Storage
} from '@ionic/storage';

@Component({
  selector: 'app-modal-order',
  templateUrl: './modal-order.page.html',
  styleUrls: ['./modal-order.page.scss'],
})
export class ModalOrderPage implements OnInit {
  @Input() tecnico: any;
  @Input() id_service: any;


  constructor(private router: Router,
    private navCtrl: NavController,
    private platform: Platform,
    private modalCtrl: ModalController,
    public loadingController: LoadingController,
    private storage: Storage,
  ) {}

  ngOnInit() {
    console.log(this.id_service);
  }

  async goToTrackOrder() {
    const loading = await this.loadingController.create({
      message: 'Cargando...',
      duration: 3000
    });
    await loading.present();
    this.modalCtrl.dismiss();
    // Data que enviaremos
    let data = {
      "listService": {
        "id": this.id_service,
        "status": 0
      }
    }
    let navigationExtras: NavigationExtras = {
      queryParams: {
        // dataCard: Nombre que indicamos que sera recibido en formularioinformacion
        // JSON.stringify(data): Recibe data y la convierte en un JSON
        dataCard: JSON.stringify(data)
      }
    };
    // Ruta de Page, NavigationExtras: Data a enviar
    this.router.navigate(['track'], navigationExtras);
    loading.dismiss();
  }

}