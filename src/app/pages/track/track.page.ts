import {
  Component,
  OnInit
} from '@angular/core';
import {
  NavController,
  Platform,
  AlertController,
  LoadingController,
  MenuController
} from '@ionic/angular';
import {
  Network
} from '@ionic-native/network/ngx';
import {
  Storage
} from '@ionic/storage';
import {
  ActivatedRoute,
  Router,
  NavigationExtras
} from '@angular/router';
import {
  UserService
} from '../../services/user/user.service';
import {
  Geolocation
} from '@ionic-native/geolocation/ngx';
import firebase from 'firebase';

declare var google;

@Component({
  selector: 'app-track',
  templateUrl: './track.page.html',
  styleUrls: ['./track.page.scss'],
})
export class TrackPage implements OnInit {
  map: any;
  directionsService = new google.maps.DirectionsService();
  directionsDisplay = new google.maps.DirectionsRenderer();
  origen : any;
  destino : any;
  messages = [];
  duracion : any;

  data: any;
  tecnicos: any;
  watch : any;
  subscription : any

  typesStatusOrder = [{
      'status': 0,
      name: 'Servicio en proceso'
    },
    {
      'status': 4,
      name: 'Servicio confirmado'
    },
    {
      'status': 3,
      name: 'Técnico se encuentra en camino a su domicilio'
    },
    {
      'status': 2,
      name: 'Tecnico se encuentra en su domicilio'
    },
    {
      'status': 1,
      name: 'Servicio terminado'
    },
  ]
  typeNetwork: string;
  token: string;
  uid: any;
  chequear: any;
  updates: any;

  statusOrder: any;
  firstName: string = "";
  secondName: string = "";
  cancelService: boolean = true;

  constructor(
    private navCtrl: NavController, private router: Router,
    private platform: Platform,
    private network: Network,
    private alertCtrl: AlertController,
    private _userProvider: UserService,
    private storage: Storage,
    public loadingController: LoadingController,
    private menu: MenuController,
    private route: ActivatedRoute,
    private geolocation: Geolocation,
  ) {}

  ngOnInit() {
    // Route recibe los parametros (se debe importar)
    this.route.queryParams.subscribe(params => {
      // Valida que exista params y params.dataCard que son los datos que recibiremos
      if (params && params.dataCard) {
        // Los parseamos nuevamente a objeto y podemos acceder a sus propiedades
        this.data = JSON.parse(params.dataCard);
        console.log(this.data);
      }
      this.statusOrder = this.data.listService.status;
      if (this.statusOrder === 0) {
        this.cancelService = true;
      } else {
        this.cancelService = false;
      }
      if (this.data.users.technical) {
        this.firstName = this.data.users.technical.firstName;
        this.secondName = this.data.users.technical.secondName;
      } else {
        this.firstName = "Tecnico no"
        this.secondName = "asginado"
      }
      this.getMessages();

      let lat =  parseFloat(this.data.listService.location.latitude)
      let long = parseFloat(this.data.listService.location.length)

      this.origen = {
        lat: lat,
        lng: long
      }
    });
  }

  async getChat(){
    console.log(this.data.listService.id)
    const uid = this.data.listService.id.toString();
    await this._userProvider.getChat(uid).then(chat => {
      const chatInfo = chat.data();
      console.log(chatInfo);
      if(chatInfo) this.openChat(uid);
      else this.errorChat();
    });
  }

  openChat(uid) {
    // Data que enviaremos
    let data = 
    {
      "id" : uid,
      "uidTec": this.data.listService.uid_technical,
      "chat": true
    }
    console.log(data)
    let navigationExtras: NavigationExtras = {
      queryParams: {
        // dataCard: Nombre que indicamos que sera recibido en formularioinformacion
        // JSON.stringify(data): Recibe data y la convierte en un JSON
        dataCard: JSON.stringify(data)
      }
    };
    // Ruta de Page, NavigationExtras: Data a enviar
    this.router.navigate(['chat'], navigationExtras);
  }

  errorChat(){
    this.alertMsj(
      "Ups! Ha ocurrido un error inesperado",
      "Por favor, intente de nuevo..."
    );
  }

  goHome() {
    this.router.navigate(['home']);
  }

  async handleGetStorageUser() {
    this.storage.get("user").then(
      user => {
        this.token = user.userToken;
        this.uid = user.userInfo.uid;

        if (user) {
          this.checkService();
        } else {
          console.log('error faltan campos de el local storage')
        }
      }
    )
  }

  async checkService() {
    this.typeNetwork = this.network.type;
    if (this.typeNetwork != 'none') {
      let data = {
        "id": this.data.listService.id
      };
      let token = this.token
      const loading = await this.loadingController.create({
        message: 'Cargando...',
        duration: 2000,
        mode: 'ios'
      });
      await loading.present();
      await this._userProvider.handlecheckService(data, token)
        .subscribe((res) => {
          this.chequear = res;
          console.log(this.chequear);
          if (this.chequear[0].listService.uid_technical === "0") {
            this.goAhead(this.chequear[0]);
          } else {
            this.alertMsj(
              "Ups! Este servicio ya fue asignado a un técnico",
              "Por favor, contacte a soporte."
            );
          }

          loading.dismiss();

        }, (err) => {
          loading.dismiss();
          this.alertMsj(
            "Ups! Ha ocurrido un error inesperado",
            "Por favor, intente de nuevo..."
          );
          console.error(err);
        });

    } else {
      this.alertMsj(
        "Error de conexión",
        "Verifica tu conexión a internet."
      );
    }
  }

  async goAhead(servicio) {
    console.log(servicio)
    const alert = await this.alertCtrl.create({
      header: 'Cancelar servicio',
      message: '¿Desea cancelar este servicio de ' + servicio.listService.service.name + '?',
      mode: 'ios',
      buttons: [{
        text: 'Cancelar',
        role: 'cancel',
        cssClass: 'secondary',
        handler: (blah) => {

        }
      }, {
        text: 'Aceptar',
        handler: () => {
          this.goService(servicio);
        }
      }]
    });

    await alert.present();
  }

  async goService(servicio) {
    this.typeNetwork = this.network.type;
    if (this.typeNetwork != 'none') {
      let data = {
        "id_service": servicio.listService.id_service,
        "uid_user": this.uid,
        "status": "1",
        "punctuation": "6"
      };
      let token = this.token
      let id_request = servicio.listService.id;
      const loading = await this.loadingController.create({
        message: 'Cargando...',
        duration: 2000,
        mode: 'ios'
      });
      await loading.present();
      await this._userProvider.handlePutService2(data, token, id_request)
        .subscribe((res) => {
          this.updates = res;
          this.storage.set('status', 4);
          this.router.navigate(['/home']);
          loading.dismiss();

        }, (err) => {
          loading.dismiss();
          this.alertMsj(
            "Ups! Ha ocurrido un error inesperado",
            "Por favor, intente de nuevo..."
          );
          console.error(err);
        });

    } else {
      this.alertMsj(
        "Error de conexión",
        "Verifica tu conexión a internet."
      );
    }
  }

  async alertMsj(header: string, message: string) {
    const alert = await this.alertCtrl.create({
      header: header,
      message: message,
      buttons: ['Ok'],
      mode: 'ios'
    });
    alert.present();
  }

  goMap() {
    // Data que enviaremos
    let data = this.data
    console.log(data)
    let navigationExtras: NavigationExtras = {
      queryParams: {
        // dataCard: Nombre que indicamos que sera recibido en formularioinformacion
        // JSON.stringify(data): Recibe data y la convierte en un JSON
        dataCard: JSON.stringify(data)
      }
    };
    // Ruta de Page, NavigationExtras: Data a enviar
    this.router.navigate(['mapadirection'], navigationExtras);
  }

  ionViewDidLeave(){
    this.subscription.unsubscribe();
  }


  // PARA CONTROL DE HORA

  async loadMap() {
      const mapEle: HTMLElement = document.getElementById('map');
      this.map = new google.maps.Map(mapEle, {
        center: this.origen,
        zoom: 13
      });
      console.log(this.map)

      this.directionsDisplay.setMap(this.map);

      google.maps.event.addListenerOnce(this.map, 'idle', () => {
        mapEle.classList.add('show-map');
        this.calculateRoute();
      });
  }

  getMessages() {
    // this.data.listService.id
    let messagesRef = firebase.database().ref("locations").child(this.data.listService.id);
    messagesRef.on("value", (snap) => {
      let data = snap.val();
      this.messages = [];
      for (var key in data) {
        this.messages.push(data[key]);
      }
      console.log(this.messages);
      this.messages.forEach(element => {
        this.destino = element;
      });
      this.loadMap();
      console.log(this.destino)
    });
  }


  private calculateRoute() {
    console.log(this.origen);
    console.log(this.destino);

    this.directionsService.route({
      origin: this.origen,
      destination: this.destino,
      travelMode: google.maps.TravelMode.WALKING,
    }, (response, status) => {
      if (status === google.maps.DirectionsStatus.OK) {
        console.log(response);
        this.duracion = response.routes[0].legs[0].duration.text;
        this.directionsDisplay.setDirections(response);
      } else {
        // alert('could not display directions due to: ' + status)
      }
    });
  }

  

}