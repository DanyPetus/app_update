import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MapadirectionPageRoutingModule } from './mapadirection-routing.module';

import { MapadirectionPage } from './mapadirection.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MapadirectionPageRoutingModule
  ],
  declarations: [MapadirectionPage]
})
export class MapadirectionPageModule {}
