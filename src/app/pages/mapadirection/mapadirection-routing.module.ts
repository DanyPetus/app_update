import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MapadirectionPage } from './mapadirection.page';

const routes: Routes = [
  {
    path: '',
    component: MapadirectionPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MapadirectionPageRoutingModule {}
