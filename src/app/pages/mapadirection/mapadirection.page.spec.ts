import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MapadirectionPage } from './mapadirection.page';

describe('MapadirectionPage', () => {
  let component: MapadirectionPage;
  let fixture: ComponentFixture<MapadirectionPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MapadirectionPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MapadirectionPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
