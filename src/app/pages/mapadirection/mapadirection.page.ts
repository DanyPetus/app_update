import {
  Component,
  OnInit,
  ViewChild,
  ElementRef
} from '@angular/core';
import {
  ActivatedRoute,
  Router,
  NavigationExtras
} from '@angular/router';
import {
  NavController,
  Platform
} from '@ionic/angular';
import {
  Geolocation
} from '@ionic-native/geolocation/ngx';
import {
  LoadingController
} from '@ionic/angular';
import {
  Subscription
} from 'rxjs';
import {
  filter
} from 'rxjs/operators';
import {
  Storage
} from '@ionic/storage';
import firebase from 'firebase';

declare var google;

@Component({
  selector: 'app-mapadirection',
  templateUrl: './mapadirection.page.html',
  styleUrls: ['./mapadirection.page.scss'],
})
export class MapadirectionPage implements OnInit {
  map: any;
  directionsService = new google.maps.DirectionsService();
  directionsDisplay = new google.maps.DirectionsRenderer();

  origin = {
    lat: 14.559381409029982,
    lng: -90.54956066581431
  };
  destination = {
    lat: 14.586944649399811,
    lng: -90.54509806227699
  };
  origen: any;
  latDes: any;
  lngDes: any;

  destino: any;
  data: any;

  messages: any;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private navCtrl: NavController,
    private platform: Platform,
    private geolocation: Geolocation,
    public loadingController: LoadingController,
    private storage: Storage
  ) {}

  ngOnInit() {
    // Route recibe los parametros (se debe importar)
    this.route.queryParams.subscribe(params => {
      // Valida que exista params y params.dataCard que son los datos que recibiremos
      if (params && params.dataCard) {
        // Los parseamos nuevamente a objeto y podemos acceder a sus propiedades
        this.data = JSON.parse(params.dataCard);
        console.log(this.data);
      }
    });
    // this.getMessages();
    this.loadMap();
  }

  async loadMap() {
    const loading = await this.loadingController.create({
      message: 'Cargando...',
      duration: 2000
    });
    await loading.present();
    console.log("si")
    let options = {
      timeout: 10000,
      enableHighAccuracy: true,
      maximumAge: 3600
    };
    this.geolocation.getCurrentPosition(options).then((resp) => {
      console.log("no")
      this.origen = {
        lat: resp.coords.latitude,
        lng: resp.coords.longitude
      }
      console.log(this.origen)
      // create a new map by passing HTMLElement
      const mapEle: HTMLElement = document.getElementById('map');
      // create map
      this.map = new google.maps.Map(mapEle, {
        center: this.origin,
        zoom: 12
      });

      this.directionsDisplay.setMap(this.map);

      google.maps.event.addListenerOnce(this.map, 'idle', () => {
        mapEle.classList.add('show-map');
        this.calculateRoute();
      });
    }).catch((error) => {
      console.log('Error getting location', error);
    });
  }


  private calculateRoute() {
    this.directionsService.route({
      origin: this.origin,
      destination: this.destination,
      travelMode: google.maps.TravelMode.DRIVING,
    }, (response, status)  => {
      if (status === google.maps.DirectionsStatus.OK) {
        this.directionsDisplay.setDirections(response);
      } else {
        alert('Could not display directions due to: ' + status);
      }
    });
    }

  // async getMessages() {
  //   // this.data.listService.id
  //   let messagesRef = firebase.database().ref("locations").child(this.data.listService.id);
  //   messagesRef.on("value", (snap) => {
  //     let data = snap.val();
  //     this.messages = [];
  //     for (var key in data) {
  //       this.messages.push(data[key]);
  //     }
  //     console.log(this.messages);
  //     this.messages.forEach(element => {
  //       this.destination = element;
  //     });
  //     console.log(this.destination)
  //   });
  // }

}