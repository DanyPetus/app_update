import {
  Component,
  OnInit,
  ViewChild
} from '@angular/core';
import firebase from 'firebase';
import {
  MenuController,
  NavController,
  LoadingController,
  AlertController,
  IonInput,
  IonContent,
  Platform,
  IonSelect
} from '@ionic/angular';
import {
  Storage
} from '@ionic/storage';
import {
  ActivatedRoute,
  Router,
  NavigationExtras
} from '@angular/router';
import {
  Camera,
  CameraOptions
} from '@ionic-native/camera/ngx';
import {
  Network
} from '@ionic-native/network/ngx';
import {
  UserService
} from '../../services/user/user.service';
import {
  FormBuilder,
  Validators,
  FormGroup
} from '@angular/forms';
import {
  AngularFireStorage
} from '@angular/fire/storage';
import {
  ImagePicker,
  ImagePickerOptions
} from '@ionic-native/image-picker/ngx';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.page.html',
  styleUrls: ['./chat.page.scss'],
})
export class ChatPage implements OnInit {
  // User Info
  name: string;
  uid: string;

  // Messages
  messageUid: any;
  messages: any;

  message: string;
  public messageForm: FormGroup;

  ///////////////////////////////////////////////////////////////

  typeNetwork: string;
  // uid: string;
  token: string;

  @ViewChild('myInput', {
    static: false
  }) myInput: IonInput;

  @ViewChild('myContent', {
    static: false
  }) myContent: IonContent;

  userName: string = "Cliente";
  // message: string = "";
  // messages = [];
  clase: string;

  nameStorage: string = "Cliente";
  data: any;

  image: any;
  album = [];
  notifications: any;
  imageBo: boolean = false
  isImg: boolean;

  mensaje: any = '';
  pathImage: any;
  arrFiles: boolean = false;
  interval: any;
  seeChat : boolean;

  constructor(
    private storage: Storage,
    private route: ActivatedRoute,
    private camera: Camera,
    private network: Network,
    public loadingController: LoadingController,
    private _userProvider: UserService,
    private alertCtrl: AlertController,
    private imagePicker: ImagePicker,
    private router: Router,
    private storageF: AngularFireStorage,
    private fb: FormBuilder,
    // private chatsService: ChatsService
  ) {}

  ngOnInit() {
    // Route recibe los parametros (se debe importar)
    this.route.queryParams.subscribe(params => {
      // Valida que exista params y params.dataCard que son los datos que recibiremos
      if (params && params.dataCard) {
        // Los parseamos nuevamente a objeto y podemos acceder a sus propiedades
        this.data = JSON.parse(params.dataCard);
        console.log(this.data);
      }
      this.messageUid = this.data.id;
      this.seeChat = this.data.chat;
      console.log(this.messageUid);
      this.handleGetStorageUser();
      // this.getChat(this.messageUid);
      // this.updateChanges(this.messageUid);
    });
    setTimeout(() => {
      this.scrollAbajo();
    }, 500);
    // this.getMessages();
  }

  async handleGetStorageUser() {
    this.storage.get("user").then(
      user => {
        let info = user.userInfo;
        this.name = info.firstName;
        this.uid = info.uid;

        if (this.uid) {
          this.initForm();
          this.getChat(this.messageUid);
          // this.updateChanges(this.messageUid);
        } else {
          console.log('error faltan campos de el local storage')
        }
      }
    )
  }

  async getChat(uid: string) {
    await this._userProvider.getChat(uid).then(chat => {
      const chatInfo = chat.data();
      this.messages = chatInfo.messages;
      console.log(this.messages)
    });
  }

  async updateChanges(uid: string) {
    this._userProvider.updateChanges(this.messageUid).onSnapshot((chat) => {
      const chatInfo = chat.data();
      this.messages = [];
      this.mensaje = '';
      this.messages = chatInfo.messages;
    });
  }

  initForm() {
    this.messageForm = this.fb.group({
      message: ['', [Validators.required]],
    });
  }

  async fileChat() {
    let indexAlbum = this.album.length;
    await this.album.forEach((element, i) => {
      // console.log(indexAlbum);
      // console.log(i);
      setTimeout(() => {
        const files = element.imagen;
        const blob = fetch(files).then(res => {
          let resBlob = res.blob().then((resBlb) => {
            console.log(resBlb);
            this.pathImage = this.createImage(resBlb).then((resPath) => {
              console.log(resPath);
              this.createMessageFile(resPath);
            });
          });
        })
      }, i * 1000);
    });
  }

  async createImage(file: any) {
    try {
      const code = Math.random().toString(36).substring(7);
      const nameImage = `${code}-${code}`
      await this.storageF.ref('/machines').child(nameImage).put(file);
      return this.storageF.ref(`machines/${nameImage}`).getDownloadURL().toPromise();
    } catch (error) {
      console.log(error);
    }
  }

  messageText(event: any) {
    this.message = event.target.value;
  }

  sendMessage() {
    // if (this.messageForm.valid) {
    console.log(this.mensaje);
    let message = this.mensaje;
    // this.patchValues();
    this.createMessage(message);
    // }
  }

  createMessage(message) {
    console.log({
      message
    })
    const messageInfo = {
      createDate: new Date(),
      message,
      name: this.name,
      uid: this.uid,
      type: 1,
    };
    console.log(messageInfo)

    this.updateChanges(this.messageUid);
    this.getChatMessageSend(messageInfo);
    setTimeout(() => {
      this.scrollAbajo();
    }, 500);
  }

  createMessageFile(path: string) {
    const messageInfo = {
      createDate: new Date(),
      path,
      name: this.name,
      uid: this.uid,
      type: 2,
    };
    console.log(messageInfo)
    this.updateChanges(this.messageUid);
    this.getChatMessageSend(messageInfo);
  }

  async getChatMessageSend(messageInfo: any) {
    await this._userProvider.getChat(this.messageUid).then((chat: any) => {
      const chatInfo = chat.data();
      chatInfo.messages.push(messageInfo);
      this.updateMessage(chatInfo);
    });
  }

  async updateMessage(chatInfo: any) {
    console.log(chatInfo);
    console.log(this.messageUid)
    this._userProvider.updateChat(this.messageUid, chatInfo).then(() => {});
    this.album = [];
    this.arrFiles = false;
  }

  patchValues() {
    this.messageForm.patchValue({
      message: '',
    });
  }

  pickImage() {
    let options: ImagePickerOptions = {
      width: 600,
      height: 600,
      quality: 50,
      outputType: 1,
      maximumImagesCount: 30
    }
    this.album = [];
    this.imagePicker.getPictures(options).then((results) => {
      for (var i = 0; i < results.length; i++) {
        let imagen = {
          imagen: 'data:image/jpeg;base64,' + results[i]
        }
        this.album.push(imagen);
      }
      this.arrFiles = true;
      console.log(this.album)
    }, (err) => {
      alert(err);
    });
  }

  verFoto(foto) {
    console.log(foto)
    // Data que enviaremos
    let data = {
      "foto": foto
    }
    let navigationExtras: NavigationExtras = {
      queryParams: {
        // dataCard: Nombre que indicamos que sera recibido en formularioinformacion
        // JSON.stringify(data): Recibe data y la convierte en un JSON
        dataCard: JSON.stringify(data)
      }
    };
    // Ruta de Page, NavigationExtras: Data a enviar
    this.router.navigate(['ficha'], navigationExtras);
  }

  // getMessages() {
  //   let messagesRef = firebase.database().ref("messages").child(this.data.id);
  //   messagesRef.on("value", (snap) => {
  //     let data = snap.val();
  //     this.messages = [];
  //     for (var key in data) {
  //       console.log(data[key])
  //       this.messages.push(data[key]);
  //     }
  //   });
  // }

  // ionViewWillEnter() {
  //   setTimeout(() => {
  //     this.scrollAbajo();
  //   }, 500);
  // }

  // sendMessage() {
  //   console.log(this.data.id)
  //   let messagesRef = firebase.database().ref("messages").child(this.data.id);
  //   messagesRef.push({
  //     message: this.message,
  //     name: this.userName
  //   });
  //   this.message = "";
  //   this.imageBo = false;
  // }

  scrollAbajo() {
    if (this.myContent.scrollToBottom) {
      this.myContent.scrollToBottom(300);
    }
  }

  // showKeyboard() {
  //   this.myInput.setFocus();
  // }

}