import {
  Component,
  OnInit
} from '@angular/core';
import {
  EmailComposer
} from '@ionic-native/email-composer/ngx';
import {
  Router,
  NavigationExtras,
  ActivatedRoute
} from '@angular/router';
import {
  NavController,
  Platform,
  AlertController,
  LoadingController,
  MenuController
} from '@ionic/angular';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.page.html',
  styleUrls: ['./forgot-password.page.scss'],
})
export class ForgotPasswordPage implements OnInit {
  email: string = "";
  plt;

  constructor(private platform: Platform,
    private emailComposer: EmailComposer,
    private route: ActivatedRoute,
    private navCtrl: NavController, private router: Router,
    private alertCtrl: AlertController,
  ) {
    if (this.platform.is('ios')) {
      this.plt = "ios";
    } else {
      this.plt = "android";
    }
  }

  ngOnInit() {}

  sendMail() {
    if (this.email === "") {
      this.alertMsj(
        "Datos incompletos",
        "Por favor, ingrese el correo electronico que tiene registrado"
      );
    } else {
      let email = {
        from: this.email,
        to: 'info@moppasa.com',
        cc: '',
        // bcc: ['john@doe.com', 'jane@doe.com'],
        attachments: [],
        subject: 'Solicitud de cambio de contraseña',
        body: 'Solicitud de cambio de contraseña al usuario de: ' + this.email,
        isHtml: true
      };
      // Send a text message using default options
      this.emailComposer.open(email);
      this.alertMsj(
        "Correo enviado satisfactoriamente",
        "Por favor, espere a que un agente de soporte técnico se contacte"
      );
      this.router.navigate(['/login']);
    }
  }

  async alertMsj(header: string, message: string) {
    const alert = await this.alertCtrl.create({
      header: header,
      message: message,
      buttons: ['Ok'],
      mode: 'ios'
    });
    alert.present();
  }

  goBack() {
    this.navCtrl.back();
  }
  goToHome() {
    this.navCtrl.back();
  }

}