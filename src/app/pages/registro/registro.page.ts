import {
  Component,
  OnInit
} from '@angular/core';
import {
  NavController,
  Platform,
  AlertController,
  LoadingController,
  MenuController
} from '@ionic/angular';
import {
  Router
} from '@angular/router';
import {
  Network
} from '@ionic-native/network/ngx';
import {
  Storage
} from '@ionic/storage';

import {
  LoginService
} from '../../services/login/login.service';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.page.html',
  styleUrls: ['./registro.page.scss'],
})
export class RegistroPage implements OnInit {
  // DATOS DE REGISTRO
  email: string = "";
  password: string = "";
  password2: string = "";
  nombre: string = ""
  apellido: string = "";
  telefono: any = "";
  nacimiento: string = "";
  genero: string = "";
  idGenero: any = "";

  typeNetwork: string;
  login: any;
  registro: any;
  check : boolean = false;

  plt
  constructor(
    private navCtrl: NavController, private router: Router,
    private platform: Platform,
    private network: Network,
    private alertCtrl: AlertController,
    private _loginProvider: LoginService,
    private storage: Storage,
    public loadingController: LoadingController,
    private menu: MenuController,
  ) {
    if (this.platform.is('ios')) {
      this.plt = "ios";
    } else {
      this.plt = "android";
    }

    this.menu.enable(false, 'first');
    this.menu.enable(false, 'second');
  }

  ngOnInit() {}

  selectFecha(ev) {
    let fecha = new Date(ev.detail.value)

    let dia = fecha.getDate(); //Tenemos 2050
    let mes = (fecha.getUTCMonth() + 1); //Tenemos 2050
    let año = fecha.getUTCFullYear(); //Tenemos 2050

    let date = dia + '/' + mes + '/' + año;
    this.nacimiento = date;
  }

  gender(ev) {
    console.log(ev.detail.value);
    this.idGenero = ev.detail.value;
  }

  goBack() {
    this.router.navigate(['/intro']);
  }

  goToLogin() {
    this.router.navigate(['/login'])
  }

  async goRegister() {
    if (this.idGenero === "1") {
      this.genero = "Masculino"
    } else if (this.idGenero === "2") {
      this.genero = "Femenino"
    } else if (this.idGenero === "3") {
      this.genero = "Sin Especificar"
    }
    this.typeNetwork = this.network.type;
    if (this.typeNetwork != 'none') {
      let data = {
        "user": {
          "email": this.email,
          "password": this.password,
        },
        "information": {
          "firstName": this.nombre,
          "secondName": this.apellido,
          "phone": this.telefono,
          "birthdate": new Date(),
          "gender": "Sin Especificar",
          "genderType": 3,
          "roleName": "Cliente",
          "roleType": 3,
          "imageProfile": "",
          "status": 1,
          "typeRegister": 1,
          "createBy": "PfNvSUQkqCSWd8Tmzxe2uwfRHhy2",
          "updateBy": "PfNvSUQkqCSWd8Tmzxe2uwfRHhy2"
        }
      }
      console.log(data);
      if (this.email === "") {
        this.alertMsj(
          "El campo EMAIL está vacío",
          "Por favor, llene todos los campos."
        );
      } else if (this.password === "") {
        this.alertMsj(
          "El campo CONTRASEÑA está vacío",
          "Por favor, llene todos los campos."
        );
      } else if (this.nombre === "") {
        this.alertMsj(
          "El campo NOMBRE está vacío",
          "Por favor, llene todos los campos."
        );
      } else if (this.apellido === "") {
        this.alertMsj(
          "El campo APELLIDO está vacío",
          "Por favor, llene todos los campos."
        );
      } else if (this.telefono === "") {
        this.alertMsj(
          "El campo TELEFONO está vacío",
          "Por favor, llene todos los campos."
        );
      } else if (this.password.length <= 5){
        this.alertMsj(
          "Contraseña insegura",
          "La contraseña tiene que tener almenos 6 digitos."
        );
      }
      await this._loginProvider.handleRegister(data)
        .subscribe((res) => {
          this.registro = res;
          this.goToHome();
        }, (err) => {
          console.error(err.status);
        });
    } else {
      this.alertMsj(
        "Error de conexión",
        "Verifica tu conexión a internet."
      );
    }
  }

  goTerminos(){
    this.router.navigate(['/terminos']);
  }

  async goToHome() {
    this.typeNetwork = this.network.type;
    if (this.typeNetwork != 'none') {
      let data = {
        'email': this.email,
        'password': this.password,
      };
      console.log(data);
      const loading = await this.loadingController.create({
        message: 'Cargando...',
        duration: 3000
      });
      await loading.present();
      await this._loginProvider.handleLog(data)
        .subscribe((res) => {
          this.login = res;
          this.storage.set('user', this.login);
          this.storage.set('status', 1);
          this.router.navigate(['/home']);
          loading.dismiss();

        }, (err) => {
          loading.dismiss();
          console.error(err.status);
        });
    } else {
      this.alertMsj(
        "Error de conexión",
        "Verifica tu conexión a internet."
      );
    }
  }

  async alertMsj(header: string, message: string) {
    const alert = await this.alertCtrl.create({
      header: header,
      message: message,
      buttons: ['Ok'],
      mode: 'ios'
    });
    alert.present();
  }

  pago(ev){
    let evento = ev.detail.checked;
    console.log(evento)
    if (evento === true){
      this.check = true;
    } else if (evento===false) {
      this.check = false;
    }
  }


}