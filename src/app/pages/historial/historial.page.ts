import {
  Component,
  OnInit
} from '@angular/core';
import {
  NavController,
  Platform,
  AlertController,
  LoadingController,
  MenuController,
  ModalController
} from '@ionic/angular';
import {
  ModalOrderPage
} from '../modal-order/modal-order.page';
import {
  Geolocation
} from '@ionic-native/geolocation/ngx';

import {
  ActivatedRoute,
  Router,
  NavigationExtras
} from '@angular/router';
// PLUGINS
import {
  Network
} from '@ionic-native/network/ngx';
import {
  Storage
} from '@ionic/storage';
import {
  UserService
} from '../../services/user/user.service';

@Component({
  selector: 'app-historial',
  templateUrl: './historial.page.html',
  styleUrls: ['./historial.page.scss'],
})
export class HistorialPage implements OnInit {
  typeNetwork: string;
  uid : string;
  token : string;

  historiales : any;
  servicios : any;

  // CALIFICACIONES
  rating1 : boolean = false;
  rating2 : boolean = false;
  rating3 : boolean = false;
  rating4 : boolean = false;
  rating5 : boolean = false;

  constructor(
    private navCtrl: NavController,
    private router: Router,
    private platform: Platform,
    private modalCtrl: ModalController,
    public loadingController: LoadingController,
    private geolocation: Geolocation,
    private route: ActivatedRoute,
    private network: Network,
    private storage: Storage,
    private alertCtrl: AlertController,
    private _userProvider: UserService,
  ) { }

  ngOnInit() {
    this.handleGetStorageUser();
  }

  async handleGetStorageUser() {
    this.storage.get("user").then(
      user => {
        this.token = user.userToken;
        this.uid = user.userInfo.uid;
        // console.log(this.uid)

        if (user) {
          this.getServices();
        } else {
          console.log('error faltan campos de el local storage')
        }
      }
    )
  }

  async getServices() {
    this.typeNetwork = this.network.type;
    if (this.typeNetwork != 'none') {
      let data = {
        "uid_user": this.uid,
        "status": 1,
      };
      let token = this.token
      // console.log(data, token);
      const loading = await this.loadingController.create({
        message: 'Cargando...',
        duration: 3000
      });
      await loading.present();
      await this._userProvider.handleGetServicioss(data, token)
        .subscribe((res:any) => {
          this.historiales = res.reverse();
          console.log(this.historiales);
          loading.dismiss();

        }, (err) => {
          // this.alertMsj(
          //   "Ups! Ha ocurrido un error inesperado",
          //   "Por favor, intente de nuevo..."
          // );
          loading.dismiss();
          console.error(err);
        });
      
    } else {
      this.alertMsj(
        "Error de conexión",
        "Verifica tu conexión a internet."
      );
    }
  }

  async alertMsj(header: string, message: string) {
    const alert = await this.alertCtrl.create({
      header: header,
      message: message,
      buttons: ['Ok'],
      mode: 'ios'
    });
    alert.present();
  }

  //// REPEAT SERVICE

  goServices(servicio){
    console.log(servicio)
    let data = {
      "name": servicio.location.name,
      "service": servicio.service,
      "location": servicio.location,
      "total1": servicio.total,
      "total": servicio.total,
      "cargos": servicio.charges,
      "transporte": servicio.transport,
      "rooms": servicio.rooms,
      "baños": servicio.bathrooms,
      "roomsPrice": servicio.rooms_price,
      "bañosPrice": servicio.bathrooms_price,
      "comentario": servicio.commentary
    }
    let navigationExtras: NavigationExtras = {
      queryParams: {
        // dataCard: Nombre que indicamos que sera recibido en formularioinformacion
        // JSON.stringify(data): Recibe data y la convierte en un JSON
        dataCard: JSON.stringify(data)
      }
    };
    // Ruta de Page, NavigationExtras: Data a enviar
    this.router.navigate(['pago'], navigationExtras);
  }

  async goChat(servicio){
    console.log(servicio);
    const uid = servicio.id.toString();
    await this._userProvider.getChat(uid).then(chat => {
      const chatInfo = chat.data();
      console.log(chatInfo);
      if(chatInfo) this.openChat(uid);
      else this.errorChat();
    });
  }

  openChat(uid) {
    // Data que enviaremos
    let data = 
    {
      "id" : uid,
      "chat": false
    }
    console.log(data)
    let navigationExtras: NavigationExtras = {
      queryParams: {
        // dataCard: Nombre que indicamos que sera recibido en formularioinformacion
        // JSON.stringify(data): Recibe data y la convierte en un JSON
        dataCard: JSON.stringify(data)
      }
    };
    // Ruta de Page, NavigationExtras: Data a enviar
    this.router.navigate(['chat'], navigationExtras);
  }

  errorChat(){
    this.alertMsj(
      "Ups! Ha ocurrido un error inesperado",
      "Por favor, intente de nuevo..."
    );
  }

}
