import {
  Component,
  OnInit,
  ViewChild
} from '@angular/core';
import {
  ActivatedRoute,
  Router,
  NavigationExtras
} from '@angular/router';
import {
  MenuController,
  NavController,
  LoadingController,
  AlertController
} from '@ionic/angular';
// PLUGINS
import {
  Network
} from '@ionic-native/network/ngx';
import {
  Storage
} from '@ionic/storage';

@Component({
  selector: 'app-calculos',
  templateUrl: './calculos.page.html',
  styleUrls: ['./calculos.page.scss'],
})
export class CalculosPage implements OnInit {
  acordionExapanded = false;
  @ViewChild("cc", {
    read: true,
    static: false
  }) CardContent: any;
  i: any

  servicio: any;

  cont: number = 0;
  arrService = [];
  services: any;
  tarifas: any;
  cantidad: any;

  totalBanos: any = 0;
  totalHabitaciones: any = 0;
  total: any;
  total1: any;

  rooms: any = 0;
  bathrooms: any = 0;
  rooms_price: any = 0;
  bathrooms_price: any = 0;

  price: any;
  totalBoo: boolean = true;

  pagoTransporte: any = 50;
  checked: boolean = false;
  checkValor: any = 30;
  checkValor2: any = 0;

  transporte: number = 0;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private storage: Storage,
    private alertCtrl: AlertController,
  ) {}

  ngOnInit() {

    let accItem = document.getElementsByClassName('accordionItem');
    let accHD = document.getElementsByClassName('accordionItemHeading');
    for (this.i = 0; this.i < accHD.length; this.i++) {
      accHD[this.i].addEventListener('click', toggleItem, false);
    }

    function toggleItem() {
      let itemClass = this.parentNode.className;
      for (this.i = 0; this.i < accItem.length; this.i++) {
        accItem[this.i].className = 'accordionItem close';
      }
      if (itemClass == 'accordionItem close') {
        this.parentNode.className = 'accordionItem open';
      }
    }

    // Route recibe los parametros (se debe importar)
    this.route.queryParams.subscribe(params => {
      // Valida que exista params y params.dataCard que son los datos que recibiremos
      if (params && params.dataCard) {
        // Los parseamos nuevamente a objeto y podemos acceder a sus propiedades
        this.servicio = JSON.parse(params.dataCard);
        console.log(this.servicio);
      }
    });
    // this.tarifas = this.servicio.tarrifs;
    this.countProducts(this.servicio.tarrifs);
    this.total = this.servicio.price;
    this.total1 = this.total;


  }

  countProducts(servicio) {
    const newTarifas = servicio.map((tarifa) => {
      tarifa.products = 0;
      return tarifa;
    });
    this.tarifas = newTarifas;
    this.cantidad = this.tarifas.length;
  }

  addRooms(precio, i) {
    this.arrService.push(precio);
    this.cont = this.arrService.length;
    this.tarifas[i].products++
    console.log(this.tarifas)

    this.rooms = this.tarifas[0].products;
    this.bathrooms = this.tarifas[1].products;

    this.rooms_price = this.tarifas[0].price;
    this.bathrooms_price = this.tarifas[1].price;

    let precio1 = this.tarifas[0].price;
    let producto1 = this.tarifas[0].products;
    this.totalHabitaciones = precio1 * producto1

    let precio2 = this.tarifas[1].price;
    let producto2 = this.tarifas[1].products;
    this.totalBanos = precio2 * producto2
    let total = this.totalBanos + this.totalHabitaciones

    if (this.checked === true) {
      this.total1 = this.total + this.totalHabitaciones + this.totalBanos + this.checkValor;
      // this.total1 = this.total1 + this.checkValor;
    } else {
      this.total1 = this.total + this.totalHabitaciones + this.totalBanos;
      // this.total1 = this.total1 - this.checkValor;
    }
    // this.total1 = this.total + this.totalHabitaciones + this.totalBanos;
  }

  goTotal() {
    this.totalBoo = false;
    this.total1 = this.total + this.totalBanos + this.totalHabitaciones;
  }

  // backTotal(){
  //   this.totalBoo = true;
  // }

  removeRooms(precio, i) {
    const index = this.arrService.findIndex(x => x.id === precio.id);
    this.arrService.splice(index, 1)
    this.tarifas[i].products--
    this.cont = this.arrService.length;
    console.log(this.arrService)

    this.rooms = this.tarifas[0].products;
    this.bathrooms = this.tarifas[1].products;

    this.rooms_price = this.tarifas[0].price;
    this.bathrooms_price = this.tarifas[1].price;

    let precio1 = this.tarifas[0].price;
    let producto1 = this.tarifas[0].products;
    this.totalHabitaciones = precio1 * producto1

    let precio2 = this.tarifas[1].price;
    let producto2 = this.tarifas[1].products;
    this.totalBanos = precio2 * producto2

    if (this.checked === true) {
      this.total1 = this.total + this.totalHabitaciones + this.totalBanos + this.checkValor;
      // this.total1 = this.total1 + this.checkValor;
    } else {
      this.total1 = this.total + this.totalHabitaciones + this.totalBanos;
      // this.total1 = this.total1 - this.checkValor;
    }
    // this.total1 = this.total + this.totalHabitaciones + this.totalBanos;
  }

  calcular() {
    let precio1 = this.tarifas[0].price;
    let producto1 = this.tarifas[0].products;
    this.totalBanos = precio1 * producto1
  }

  pago(ev) {
    this.checked = ev.detail.checked;
    if (this.checked === true) {
      this.total1 = this.total1 + this.checkValor;
    } else {
      this.total1 = this.total1 - this.checkValor;
    }
  }

  goCheck() {
    this.storage.get("status").then(
      status => {
        if (status === 0) {
          this.presentAlertConfirm();
        } else {
          if (this.totalHabitaciones && this.totalBanos === 0) {
            this.total = this.servicio.price;
          } else {

          }
          if (this.checked === true) {
            this.transporte = 1
            this.checkValor2 = 30;
          } else if (this.checked === false) {
            this.transporte = 0
            this.checkValor2 = 0;
          }
          // Data que enviaremos
          let data = {
            "service": this.servicio,
            "total1": this.total1,
            "total": this.total,
            "cargos": this.totalBanos + this.totalHabitaciones + this.checkValor2,
            "transporte": this.transporte,
            "rooms": this.rooms,
            "baños": this.bathrooms,
            "roomsPrice": this.rooms_price,
            "bañosPrice": this.bathrooms_price
          }
          let navigationExtras: NavigationExtras = {
            queryParams: {
              // dataCard: Nombre que indicamos que sera recibido en formularioinformacion
              // JSON.stringify(data): Recibe data y la convierte en un JSON
              dataCard: JSON.stringify(data)
            }
          };
          // Ruta de Page, NavigationExtras: Data a enviar
          this.router.navigate(['checkout'], navigationExtras);
        }
      })
  }

  async presentAlertConfirm() {
    const alert = await this.alertCtrl.create({
      cssClass: 'my-custom-class',
      header: 'Iniciar sesión',
      mode: 'ios',
      message: 'Para solicitar un servicio necesitas iniciar sesión',
      buttons: [{
        text: 'Cancelar',
        role: 'cancel',
        cssClass: 'secondary',
        handler: (blah) => {
          console.log('Confirm Cancel: blah');
        }
      }, {
        text: 'Login',
        handler: () => {
          this.router.navigate(['login']);
        }
      }]
    });

    await alert.present();
  }

}