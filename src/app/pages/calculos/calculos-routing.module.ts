import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CalculosPage } from './calculos.page';

const routes: Routes = [
  {
    path: '',
    component: CalculosPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CalculosPageRoutingModule {}
