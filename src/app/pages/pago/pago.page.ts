import {
  Component,
  OnInit
} from '@angular/core';
import {
  NavController,
  Platform,
  AlertController,
  LoadingController,
  MenuController,
  ModalController
} from '@ionic/angular';
import {
  ActivatedRoute,
  Router,
  NavigationExtras
} from '@angular/router';
// PLUGINS
import {
  Network
} from '@ionic-native/network/ngx';
import {
  Storage
} from '@ionic/storage';
import {
  UserService
} from '../../services/user/user.service';
import {
  ModalOrderPage
} from '../modal-order/modal-order.page';
import * as moment from 'moment';

@Component({
  selector: 'app-pago',
  templateUrl: './pago.page.html',
  styleUrls: ['./pago.page.scss'],
})
export class PagoPage implements OnInit {
  idPay: number = 1;
  typeNetwork: string;
  data: any;
  token: string;
  uid: string;
  comentario: string = "";
  createService: any;

  mostrar: boolean = false;
  programarID: number = 0;
  seeCards: boolean = false;

  card = {
    cardName: "",
    accountNumber: "",
    expirationMonth: "",
    expirationYear: "",
    Cvv: "",
    typeCard: ""
  }

  pagos: any;

  roleType: string;
  firstName: string;
  secondName: string;
  email: string;
  phone: string;
  parseMonth: any;
  parseYear: any;

  fechaProgramada: any = 0;
  horaProgramada: any = 0;

  constructor(
    private navCtrl: NavController,
    private router: Router,
    private platform: Platform,
    private modalCtrl: ModalController,
    public loadingController: LoadingController,
    private route: ActivatedRoute,
    private network: Network,
    private storage: Storage,
    private alertCtrl: AlertController,
    private _userProvider: UserService,
  ) {}

  ngOnInit() {
    this.card.expirationMonth = moment().format();
    this.card.expirationYear = moment().format('YYYY');

    // Route recibe los parametros (se debe importar)
    this.route.queryParams.subscribe(params => {
      // Valida que exista params y params.dataCard que son los datos que recibiremos
      if (params && params.dataCard) {
        // Los parseamos nuevamente a objeto y podemos acceder a sus propiedades
        this.data = JSON.parse(params.dataCard);
        console.log(this.data);
      }
      this.comentario = this.data.comentario;
    });
  }

  getTypeCard() {
    if (document.querySelectorAll('.visa').length !== 0) {
      this.card.typeCard = 'visa';
    } else if (document.querySelectorAll('.mastercard').length !== 0) {
      this.card.typeCard = 'master';
    }
  }

  choosePaymentMethod(value) {
    this.idPay = value;
    if (this.idPay === 1) {
      this.seeCards = false;
    } else {
      this.seeCards = true;
    }
  }

  async handleGetStorageUser() {
    if (this.fechaProgramada === 0 && this.programarID === 1) {
      this.alertMsj(
        "Fecha del servicio incompleta",
        "Favor colocar la fecha que desea programar el servicio"
      );
    } else if (this.horaProgramada === 0 && this.programarID === 1) {
      this.alertMsj(
        "Hora del servicio incompleta",
        "Favor colocar la hora que desea programar el servicio"
      );
    } else {
      this.storage.get("user").then(
        user => {
          this.token = user.userToken;
          this.uid = user.userInfo.uid;
          this.roleType = JSON.parse(user.userInfo.roleType);
          this.firstName = user.userInfo.firstName;
          this.secondName = user.userInfo.secondName;
          this.email = user.userInfo.email;
          this.phone = user.userInfo.phone;

          if (this.idPay === 1) {
            this.getServices();
          } else if (this.idPay === 2) {
            this.getPay();
          } else {
            console.log('error faltan campos de el local storage')
          }
        }
      )
    }
  }

  async getServices() {
    var today = new Date();
    var h = today.getHours();
    var m = today.getMinutes();
    var s = today.getSeconds();
    let hora = (h + ":" + m + ":" + s)
    let tiempo
    let fecha
    if (this.programarID === 1) {
      fecha = this.fechaProgramada;
      tiempo = this.horaProgramada;
    } else {
      fecha = today;
      tiempo = hora;
    }
    this.typeNetwork = this.network.type;
    if (this.typeNetwork != 'none') {
      let data = {
        "punctuation": 0,
        "id_service": this.data.service.id,
        "uid_user": this.uid,
        "uid_technical": "0",
        "id_location": this.data.location.id,
        "status": "5",
        "invoice": "",
        "programmed": this.programarID,
        "date": fecha,
        "time": tiempo,
        "products": 1,
        "commentary": this.comentario,
        "transport": this.data.transporte,
        "transportation_charge": this.data.transporte,
        "service_price": this.data.total1,
        "charges": this.data.cargos,
        "total": this.data.total1,
        "type_payment": this.idPay,
        "rooms": this.data.rooms,
        "bathrooms": this.data.baños,
        "rooms_price": this.data.roomsPrice,
        "bathrooms_price": this.data.bañosPrice
      };
      let token = this.token
      console.log(data, token);
      const loading = await this.loadingController.create({
        message: 'Cargando...',
        duration: 3000
      });
      await loading.present();
      await this._userProvider.handlePostService(data, token)
        .subscribe((res) => {
          this.createService = res;
          console.log("SERVICIO", this.createService);
          if (this.programarID === 1) {
            this.router.navigate(['home']);
            this.alertMsj(
              "¡Servicio programado con éxito!",
              "Se ha programado un nuevo servicio"
            );
            loading.dismiss();
            this.goToSendOrder(this.createService.id);
          } else if (this.programarID === 0) {
            this.alertMsj(
              "Servicio creado con éxito!",
              "Se ha creado un nuevo servicio"
            );
            loading.dismiss();
            this.goToSendOrder(this.createService.id);
          }

        }, (err) => {
          this.alertMsj(
            "Ups! Ha ocurrido un error inesperado",
            "Por favor, intente de nuevo..."
          );
          loading.dismiss();
          console.error(err);
        });
    } else {
      this.alertMsj(
        "Error de conexión",
        "Verifica tu conexión a internet."
      );
    }
  }

  async alertMsj(header: string, message: string) {
    const alert = await this.alertCtrl.create({
      header: header,
      message: message,
      buttons: ['Ok'],
      mode: 'ios'
    });
    alert.present();
  }

  programar(ev) {
    let evento = ev.detail.value;
    if (evento === "1") {
      this.mostrar = true;
      this.programarID = 1;
    } else if (evento === "0") {
      this.mostrar = false;
      this.programarID = 0;
    }
  }

  async goToSendOrder(id) {
    const loading = await this.loadingController.create({
      message: 'Cargando...',
      duration: 3000
    });
    await loading.present();
    const modal = await this.modalCtrl.create({
      component: ModalOrderPage,
      componentProps: {
        'id_service': id
      },
      cssClass: 'modalorder'
    });
    loading.dismiss();
    return await modal.present();
  }


  seeDate(ev) {
    let datePicker = ev.detail.value;
    this.fechaProgramada = new Date(datePicker);
    console.log(this.fechaProgramada);
  }

  seeHour(ev) {
    let hourPicker = ev.detail.value;
    let hour = new Date(hourPicker)
    console.log(ev)
    var h = hour.getHours();
    var m = hour.getMinutes();
    var s = hour.getSeconds();
    this.horaProgramada = (h + ":" + m + ":" + s)
    console.log(this.horaProgramada)
  }

  // PAGALO CHECK

  async getPay() {
    let service = new Number(this.data.service.id);
    let idService = service.toString();
    let codigo = new Number(this.roleType);
    let code = codigo.toString();
    let phone = new Number(this.phone);
    let cel = phone.toString();
    let mes = new Date(this.card.expirationMonth);
    this.parseMonth = (mes.getMonth() + 1);
    let parseMonth = new Number(this.parseMonth);
    console.log(parseMonth);
    let parseMonth2
    if (parseMonth < 10) {
      parseMonth2 = "0" + parseMonth
    } else {
      parseMonth2 = parseMonth
    }
    let parseMonth1 = parseMonth2.toString();
    console.log(parseMonth1);
    let anio = new Date(this.card.expirationYear);
    this.parseYear = anio.getFullYear();
    let parseYear = new Number(this.parseYear);
    let parseYear1 = parseYear.toString();
    let total = new Number(this.data.total1);
    let total1 = total.toString();
    let del = this.card.accountNumber.replace(/-/g, "");
    console.log(del);
    if (this.typeNetwork != 'none') {
      let empresa = {
        "key_secret": "5gT0mHpmCkufcNCJ8OG4gEpN6Q4gHlq4lW0KbHt9",
        "key_public": "VPfHFybCPNuDMEs8RHlwPIQJ7oK5zph5NzeYVuXg",
        "idenEmpresa": "M994480910",
      }
      let cliente = {
        // "codigo": code,
        "firstName": this.firstName,
        "lastName": this.secondName,
        "street1": this.data.name,
        "country": "GT",
        "city": "Guatemala",
        "state": "GT",
        "email": this.email,
        "ipAddress": "",
        "Total": total1 + ".00",
        "currency": "GTQ",
        "fecha_transaccion": null,
        "postalCode": "01009",
        "phone": cel,
        // "deviceFingerprintID": ""
      }
      let tarjetaPagalo = {
        "nameCard": this.card.cardName,
        "accountNumber": del,
        "expirationMonth": parseMonth1,
        "expirationYear": parseYear1,
        "CVVCard": this.card.Cvv
      }
      let detalle = [{
        "id_producto": idService,
        "cantidad": "1",
        "tipo": "producto",
        "nombre": this.data.service.name,
        "precio": total1 + ".00",
        "Subtotal": total1 + ".00"
      }]
      let empresa1 = JSON.stringify(empresa);
      let cliente1 = JSON.stringify(cliente);
      let tarjetaPagalo1 = JSON.stringify(tarjetaPagalo);
      let detalle1 = JSON.stringify(detalle);


      let data = {
        "empresa": empresa1,
        "cliente": cliente1,
        "detalle": detalle1,
        "tarjetaPagalo": tarjetaPagalo1,
      }
      let datos = JSON.stringify(data);
      console.log(datos);

      const loading = await this.loadingController.create({
        message: 'Cargando...',
        duration: 3000
      });
      await loading.present();
      await this._userProvider.handlePay(datos)
        .subscribe((res) => {
          this.pagos = res;
          console.log(this.pagos);
          if (this.pagos.decision === "REJECT") {
            this.alertMsj(
              "Ups! Ha ocurrido un error inesperado",
              "Por favor, intente de nuevo..."
            );
          } else {
            this.getServices();
          }
          loading.dismiss();

        }, (err) => {
          this.alertMsj(
            "Ups! Ha ocurrido un error inesperado",
            "Por favor, intente de nuevo..."
          );
          loading.dismiss();
          console.error(err);
        });
    } else {
      this.alertMsj(
        "Error de conexión",
        "Verifica tu conexión a internet."
      );
    }
  }


}