import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PagoPageRoutingModule } from './pago-routing.module';

import { PagoPage } from './pago.page';
import { ReactiveFormsModule } from '@angular/forms';

import { BrMaskerModule } from 'br-mask';
import { CreditCardDirectivesModule } from 'angular-cc-library';

@NgModule({
  imports: [
    CreditCardDirectivesModule,
    ReactiveFormsModule,
    CommonModule,
    FormsModule,
    IonicModule,
    PagoPageRoutingModule,
    BrMaskerModule
  ],
  declarations: [PagoPage]
})
export class PagoPageModule {}
