import {
  Component,
  OnInit,
  ViewChild
} from '@angular/core';
import {
  LoadingController,
} from '@ionic/angular';
import {
  ActivatedRoute,
} from '@angular/router';

@Component({
  selector: 'app-ficha',
  templateUrl: './ficha.page.html',
  styleUrls: ['./ficha.page.scss'],
})
export class FichaPage implements OnInit {
  data : any;

  constructor(
    private route: ActivatedRoute,
    public loadingController: LoadingController,
  ) {}

  ngOnInit() {
    // Route recibe los parametros (se debe importar)
    this.route.queryParams.subscribe(params => {
      // Valida que exista params y params.dataCard que son los datos que recibiremos
      if (params && params.dataCard) {
        // Los parseamos nuevamente a objeto y podemos acceder a sus propiedades
        this.data = JSON.parse(params.dataCard);
        console.log(this.data);
      }
    });
  }

}