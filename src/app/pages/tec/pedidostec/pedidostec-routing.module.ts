import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PedidostecPage } from './pedidostec.page';

const routes: Routes = [
  {
    path: '',
    component: PedidostecPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PedidostecPageRoutingModule {}
