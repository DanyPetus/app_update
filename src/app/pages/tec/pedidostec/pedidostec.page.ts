import {
  Component,
  OnInit
} from '@angular/core';
import {
  NavController,
  Platform,
  AlertController,
  LoadingController,
  MenuController,
  ModalController
} from '@ionic/angular';
import {
  Geolocation
} from '@ionic-native/geolocation/ngx';

import {
  ActivatedRoute,
  Router,
  NavigationExtras
} from '@angular/router';
// PLUGINS
import {
  Network
} from '@ionic-native/network/ngx';
import {
  Storage
} from '@ionic/storage';
import {
  UserService
} from '../../../services/user/user.service';

@Component({
  selector: 'app-pedidostec',
  templateUrl: './pedidostec.page.html',
  styleUrls: ['./pedidostec.page.scss'],
})
export class PedidostecPage implements OnInit {
  typeNetwork: string;
  uid : string;
  token : string;

  historiales : any;
  servicios : any;

  constructor(
    private navCtrl: NavController,
    private router: Router,
    private platform: Platform,
    private modalCtrl: ModalController,
    public loadingController: LoadingController,
    private geolocation: Geolocation,
    private route: ActivatedRoute,
    private network: Network,
    private storage: Storage,
    private alertCtrl: AlertController,
    private _userProvider: UserService,
  ) { }

  ngOnInit() {
    this.handleGetStorageUser();
  }

  async handleGetStorageUser() {
    this.storage.get("user").then(
      user => {
        this.token = user.userToken;
        this.uid = user.userInfo.uid;
        console.log(this.uid)

        if (user) {
          this.getServices();
        } else {
          console.log('error faltan campos de el local storage')
        }
      }
    )
  }

  async getServices() {
    this.typeNetwork = this.network.type;
    if (this.typeNetwork != 'none') {
      let data = {
        "uid_technical": this.uid,
        "status": 1,
      };
      let token = this.token
      console.log(data, token);
      const loading = await this.loadingController.create({
        message: 'Cargando...',
        duration: 3000
      });
      await loading.present();
      await this._userProvider.handleGetServicioss(data, token)
        .subscribe((res) => {
          this.historiales = res;
          console.log(this.historiales);
          loading.dismiss();

        }, (err) => {
          this.alertMsj(
            "Ups! Ha ocurrido un error inesperado",
            "Por favor, intente de nuevo..."
          );
          loading.dismiss();
          console.error(err);
        });
      
    } else {
      this.alertMsj(
        "Error de conexión",
        "Verifica tu conexión a internet."
      );
    }
  }

  async alertMsj(header: string, message: string) {
    const alert = await this.alertCtrl.create({
      header: header,
      message: message,
      buttons: ['Ok'],
      mode: 'ios'
    });
    alert.present();
  }

}
