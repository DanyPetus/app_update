import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PedidostecPageRoutingModule } from './pedidostec-routing.module';

import { PedidostecPage } from './pedidostec.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PedidostecPageRoutingModule
  ],
  declarations: [PedidostecPage]
})
export class PedidostecPageModule {}
