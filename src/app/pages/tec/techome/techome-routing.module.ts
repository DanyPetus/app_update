import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TechomePage } from './techome.page';

const routes: Routes = [
  {
    path: '',
    component: TechomePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TechomePageRoutingModule {}
