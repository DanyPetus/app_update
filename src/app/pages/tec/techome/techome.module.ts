import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TechomePageRoutingModule } from './techome-routing.module';

import { TechomePage } from './techome.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TechomePageRoutingModule
  ],
  declarations: [TechomePage]
})
export class TechomePageModule {}
