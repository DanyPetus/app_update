import {
  Component,
  OnInit
} from '@angular/core';
import {
  NavController,
  Platform,
  AlertController,
  LoadingController,
  MenuController
} from '@ionic/angular';
import {
  Network
} from '@ionic-native/network/ngx';
import {
  Storage
} from '@ionic/storage';
import {
  ActivatedRoute,
  Router,
  NavigationExtras
} from '@angular/router';
import {
  UserService
} from '../../../services/user/user.service';
import {
  LaunchNavigator,
  LaunchNavigatorOptions
} from '@ionic-native/launch-navigator/ngx';
import {
  Geolocation
} from '@ionic-native/geolocation/ngx';

@Component({
  selector: 'app-techome',
  templateUrl: './techome.page.html',
  styleUrls: ['./techome.page.scss'],
})
export class TechomePage implements OnInit {
  typeNetwork: string;
  servicios: any;
  token: any;
  uid: string;
  procesos: any;
  mostrar: boolean = false;

  updates: any;
  chequear: any;
  status: any;
  listadosFilter: any;
  listadoServicios: any = [];
  totalkm : any;

  constructor(
    private navCtrl: NavController, private router: Router,
    private platform: Platform,
    private network: Network,
    private alertCtrl: AlertController,
    private _userProvider: UserService,
    private storage: Storage,
    public loadingController: LoadingController,
    private menu: MenuController,
    private launchNavigator: LaunchNavigator,
    private geolocation: Geolocation
  ) {}

  ionViewWillEnter() {
    this.openFirst()
    this.handleGetStorageStatus();
    this.handleGetStorageUser();
  }

  ngOnInit() {
    // this.openFirst()
    // this.handleGetStorageStatus();
    // this.handleGetStorageUser();
  }

  openFirst() {
    this.menu.enable(false, 'first');
    this.menu.enable(true, 'second');
  }

  buscar(ev) {
    let val = ev.detail.value;
    if (val && val.trim() !== '') {
      this.listadoServicios = this.procesos.filter(term =>
        term.service.name.toLowerCase().indexOf(val.toLowerCase().trim()) > -1)
    } else {
      this.listadoServicios = this.procesos;
      console.log(this.listadoServicios)
    }
  }


  async handleGetStorageStatus() {
    this.storage.get("status").then(
      user => {
        this.status = user;
        if (this.status === 4) {
          this.router.navigate(['/serviceatendido']);
        } else if (this.status === 3) {
          this.router.navigate(['/serviceatendido']);
        } else if (this.status === 2) {
          this.router.navigate(['/serviceatendido']);
        } else if (this.status === 1) {
          this.router.navigate(['/techome']);
        } else {
          console.log('error faltan campos de el local storage')
        }
      }
    )
  }

  doRefresh(event) {
    this.listadoServicios = [];
    this.handleGetStorageUser();
    setTimeout(() => {
      // console.log('Async operation has ended');
      event.target.complete();
    }, 2000);
  }

  async handleGetStorageUser() {
    this.storage.get("user").then(
      user => {
        this.token = user.userToken;
        this.uid = user.userInfo.uid;

        if (user) {
          this.getProcesos();
        } else {
          console.log('error faltan campos de el local storage')
        }
      }
    )
  }

  async getProcesos() {
    this.typeNetwork = this.network.type;
    if (this.typeNetwork != 'none') {
      let data = {
        "status": "5",
      };
      let token = this.token
      await this._userProvider.handleGetServicioss(data, token)
        .subscribe((res) => {
          this.procesos = res;
          this.listadosFilter = this.procesos;
          if (this.procesos) {
            this.mostrar = true
          }
          this.getDistance();
          // console.log(this.procesos);

        }, (err) => {
          this.alertMsj(
            "Ups! Ha ocurrido un error inesperado",
            "Por favor, intente de nuevo..."
          );
          console.error(err);
        });

    } else {
      this.alertMsj(
        "Error de conexión",
        "Verifica tu conexión a internet."
      );
    }
  }

  async getDistance() {
    const loading = await this.loadingController.create({
      message: 'Cargando...',
      duration: 2000,
      mode: 'ios'
    });
    await loading.present();
    this.geolocation.getCurrentPosition().then((resp) => {
      let lat_tech = resp.coords.latitude;
      let lng_tech = resp.coords.longitude;

      this.listadosFilter.forEach(element => {
        let lat_user = element.listService.location.latitude;
        let lng_user = element.listService.location.length;

        this.totalkm = this.calculateDistance(lng_tech, lng_user, lat_tech, lat_user);
        console.log(this.totalkm);
        if (this.totalkm <= 15){
          this.listadoServicios.push(element);
        }
      });
      console.log(this.listadoServicios);
      loading.dismiss();

    }).catch((error) => {
      loading.dismiss();
      console.log('Error getting location', error);
    });

  }

  calculateDistance(lon1, lon2, lat1, lat2){
    let p = 0.017453292519943295;
    let c = Math.cos;
    let a = 0.5 - c((lat1-lat2) * p) / 2 + c(lat2 * p) *c((lat1) * p) * (1 - c(((lon1- lon2) * p))) / 2;
    let dis = (12742 * Math.asin(Math.sqrt(a)));
    return Math.trunc(dis);
}

  async goService(servicio) {
    this.typeNetwork = this.network.type;
    if (this.typeNetwork != 'none') {
      let data = {
        "id_service": servicio.listService.id_service,
        "uid_technical": this.uid,
        "status": "4"
      };
      let token = this.token
      let id_request = servicio.listService.id;
      const loading = await this.loadingController.create({
        message: 'Cargando...',
        duration: 2000,
        mode: 'ios'
      });
      await loading.present();
      await this._userProvider.handlePutService2(data, token, id_request)
        .subscribe((res) => {
          this.updates = res;
          this.storage.set('status', 4);
          this.storage.set('service', servicio);
          this.router.navigate(['/serviceatendido']);
          loading.dismiss();
          console.log(this.procesos);

        }, (err) => {
          loading.dismiss();
          this.alertMsj(
            "Ups! Ha ocurrido un error inesperado",
            "Por favor, intente de nuevo..."
          );
          console.error(err);
        });

    } else {
      this.alertMsj(
        "Error de conexión",
        "Verifica tu conexión a internet."
      );
    }
  }

  async checkService(servicio) {
    this.typeNetwork = this.network.type;
    if (this.typeNetwork != 'none') {
      let data = {
        "id": servicio.listService.id
      };
      let token = this.token
      const loading = await this.loadingController.create({
        message: 'Cargando...',
        duration: 2000,
        mode: 'ios'
      });
      await loading.present();
      await this._userProvider.handlecheckService(data, token)
        .subscribe((res) => {
          this.chequear = res;
          console.log(this.chequear);
          if (this.chequear[0].listService.uid_technical === "0") {
            this.goAhead(servicio);
          } else {
            this.alertMsj(
              "Ups! Este servicio ya fue asignado anteriormente",
              "Por favor, refresque su inicio."
            );
          }

          loading.dismiss();

        }, (err) => {
          loading.dismiss();
          this.alertMsj(
            "Ups! Ha ocurrido un error inesperado",
            "Por favor, intente de nuevo..."
          );
          console.error(err);
        });

    } else {
      this.alertMsj(
        "Error de conexión",
        "Verifica tu conexión a internet."
      );
    }
  }

  async goAhead(servicio) {
    console.log(servicio)
    const alert = await this.alertCtrl.create({
      header: 'Aceptar servicio',
      message: '¿Desea aceptar este servicio de ' + servicio.listService.service.name + '?',
      mode: 'ios',
      buttons: [{
        text: 'Cancelar',
        role: 'cancel',
        cssClass: 'secondary',
        handler: (blah) => {

        }
      }, {
        text: 'Ver ubicación',
        // role: 'cancel',
        cssClass: 'secondary',
        handler: () => {
          this.goWaze(servicio.listService.location.latitude, servicio.listService.location.length);
        }
      }, {
        text: 'Aceptar',
        handler: () => {
          this.goService(servicio);
        }
      }]
    });

    await alert.present();
  }

  async goWaze(lat_user, lng_user) {
    const loading = await this.loadingController.create({
      message: 'Cargando...',
      duration: 3000
    });
    await loading.present();
    this.geolocation.getCurrentPosition().then((resp) => {
      console.log(resp.coords.latitude)
      console.log(resp.coords.longitude)
      let lat = parseFloat(lat_user);
      let long = parseFloat(lng_user);
      console.log(lat);
      console.log(long);
      let options: LaunchNavigatorOptions = {
        start: [resp.coords.latitude, resp.coords.longitude],
        appSelection: {
          dialogHeaderText: "Seleccione la aplicación de navegacion"
        }
      }

      this.launchNavigator.navigate([lat, long], options)
        .then(
          success => console.log('Launched navigator'),
          error => console.log('Error launching navigator', error)
        );

    }).catch((error) => {
      console.log('Error getting location', error);
    });
    loading.dismiss();
  }

  async alertMsj(header: string, message: string) {
    const alert = await this.alertCtrl.create({
      header: header,
      message: message,
      buttons: ['Ok'],
      mode: 'ios'
    });
    alert.present();
  }

}