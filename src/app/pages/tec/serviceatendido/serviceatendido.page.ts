import {
  Component,
  OnInit
} from '@angular/core';
import {
  NavController,
  Platform,
  AlertController,
  LoadingController,
  MenuController
} from '@ionic/angular';
import {
  Network
} from '@ionic-native/network/ngx';
import {
  Storage
} from '@ionic/storage';
import {
  ActivatedRoute,
  Router,
  NavigationExtras
} from '@angular/router';
import {
  UserService
} from '../../../services/user/user.service';
import {
  LaunchNavigator,
  LaunchNavigatorOptions
} from '@ionic-native/launch-navigator/ngx';
import {
  Geolocation
} from '@ionic-native/geolocation/ngx';
import firebase from 'firebase';


@Component({
  selector: 'app-serviceatendido',
  templateUrl: './serviceatendido.page.html',
  styleUrls: ['./serviceatendido.page.scss'],
})
export class ServiceatendidoPage implements OnInit {
  // SERVICIO
  nameService: string;
  time_services: any;
  detalleLocation: string;
  transporte: boolean;
  subTotalService: any;
  totalService: any;
  tipoPago: any;
  id: any;
  request: any;
  banios: any;
  precioBanios: any;
  cuartos: any;
  precioCuartos: any;
  charges: any;

  // STATUS BOTONES
  statusCamino: boolean = false;
  statusEmpieza: boolean = false;
  statusTermina: boolean = false;


  // CLIENTE
  nameClient: string;
  apellidoClient: string;
  phoneClient: any;

  servicio: any;
  status: any;
  updates: any;
  typeNetwork: string;
  token: any;
  uid: string;
  idUsuario: any;
  nameLocation : any;
  comentary_location : any;

  lat_user : any;
  long_user : any;

  watch : any;
  constructor(
    private navCtrl: NavController, private router: Router,
    private platform: Platform,
    private network: Network,
    private alertCtrl: AlertController,
    private _userProvider: UserService,
    private storage: Storage,
    public loadingController: LoadingController,
    private menu: MenuController,
    private launchNavigator: LaunchNavigator,
    private geolocation: Geolocation
  ) {}

  ngOnInit() {
    this.handleGetStorageStatus();
    this.storage.get("service").then(
      user => {
        this.servicio = user;
        console.log(this.servicio)
        // SERVICE
        this.nameService = user.listService.service.name;
        this.detalleLocation = user.listService.commentary;
        this.time_services = user.listService.service.time_services;
        if (user.listService.transport === 1) {
          this.transporte = true;
        } else {
          this.transporte = false;
        }
        this.totalService = user.listService.total;
        this.subTotalService = user.listService.service_price;
        if (user.listService.type_payment === 1) {
          this.tipoPago = "Efectivo"
        } else if (user.listService.type_payment === 2) {
          this.tipoPago = "Tarjeta de crédito"
        }
        this.banios = user.listService.bathrooms;
        this.precioBanios = user.listService.bathrooms_price;
        this.cuartos = user.listService.rooms;
        this.precioCuartos = user.listService.rooms_price;
        this.charges = user.listService.charges;
        this.nameLocation = user.listService.location.name;
        this.comentary_location = user.listService.location.commentary;

        // CLIENT
        this.nameClient = user.users.client.firstName;
        this.apellidoClient = user.users.client.secondName;
        this.phoneClient = user.users.client.phone;
        this.idUsuario = user.users.client.uid;

      }
    )
  }

  async getLocation() {
    this.storage.get("service").then(
      user => {
        this.lat_user = user.listService.location.latitude;
        this.long_user = user.listService.location.length;
        console.log(this.lat_user);
        console.log(this.long_user);

        if (user) {
          this.goWaze();
        } else {
          console.log('error faltan campos de el local storage')
        }
      }
    )
  }

  async goWaze() {
    const loading = await this.loadingController.create({
      message: 'Cargando...',
      duration: 3000
    });
    await loading.present();
    this.geolocation.getCurrentPosition().then((resp) => {
      // resp.coords.latitude
      // resp.coords.longitude
      console.log(resp.coords.latitude)
      console.log(resp.coords.longitude)
      let lat = parseFloat(this.lat_user);
      let long = parseFloat(this.long_user);
      console.log(lat);
      console.log(long);
      let options: LaunchNavigatorOptions = {
        start: [resp.coords.latitude, resp.coords.longitude],
        appSelection: {
          dialogHeaderText: "Seleccione la aplicación de navegacion"
        }
      }

      this.launchNavigator.navigate([lat, long], options)
        .then(
          success => console.log('Launched navigator')
          ,
          error => console.log('Error launching navigator', error)
        );

    }).catch((error) => {
      console.log('Error getting location', error);
    });
    loading.dismiss();
  }


  async handleGetStorageStatus() {
    this.storage.get("status").then(
      user => {
        this.status = user;
        console.log(this.status)

        if (this.status === 4) {
          this.statusCamino = true;
          this.statusEmpieza = false;
          this.statusTermina = false;
        } else if (this.status === 3) {
          this.statusCamino = false;
          this.statusEmpieza = true;
          this.statusTermina = false;
        } else if (this.status === 2) {
          this.statusCamino = false;
          this.statusEmpieza = false;
          this.statusTermina = true;
        } else if (this.status === 1) {
          this.router.navigate(['/techome']);
        } else {
          console.log('error faltan campos de el local storage')
        }
      }
    )
  }

  // IR EN CAMINO

  async goHome() {
    this.storage.get("service").then(
      user => {
        this.id = user.listService.id_service;
        this.request = user.listService.id;

        if (this.id) {
          this.handleGetStorageUser(this.id);
          this.sendMessage(this.request);
        } else {
          console.log('error faltan campos de el local storage')
        }
      }
    )
  }

  async handleGetStorageUser(id) {
    this.storage.get("user").then(
      user => {
        this.token = user.userToken;
        this.uid = user.userInfo.uid;

        if (user) {
          this.goService(id);
        } else {
          console.log('error faltan campos de el local storage')
        }
      }
    )
  }

  async goService(id) {
    this.typeNetwork = this.network.type;
    if (this.typeNetwork != 'none') {
      let data = {
        "id_service": id,
        "uid_technical": this.uid,
        "status": "3"
      };
      let request = this.request;
      let token = this.token
      const loading = await this.loadingController.create({
        message: 'Cargando...',
        duration: 2000,
        mode: 'ios'
      });
      await loading.present();
      await this._userProvider.handlePutService(data, token, request)
        .subscribe((res) => {
          this.updates = res;
          this.storage.set('status', 3);
          this.statusCamino = false;
          this.statusEmpieza = true;
          this.statusTermina = false;
          loading.dismiss();

        }, (err) => {
          loading.dismiss();
          this.alertMsj(
            "Ups! Ha ocurrido un error inesperado",
            "Por favor, intente de nuevo..."
          );
          console.error(err);
        });

    } else {
      this.alertMsj(
        "Error de conexión",
        "Verifica tu conexión a internet."
      );
    }
  }

  async irEnCamino() {
    const alert = await this.alertCtrl.create({
      header: 'Ir en camino',
      message: '¿Desea ir en camino a la ubicacion?',
      mode: 'ios',
      buttons: [{
        text: 'Cancelar',
        role: 'cancel',
        cssClass: 'secondary',
        handler: (blah) => {

        }
      }, {
        text: 'Aceptar',
        handler: () => {
          this.goHome();
        }
      }]
    });

    await alert.present();
  }

  // EMPEZAR TRABAJO

  async goStart() {
    this.storage.get("service").then(
      user => {
        this.id = user.listService.id_service;
        this.request = user.listService.id;

        if (this.id) {
          this.handleGetStorageStart(this.id);
        } else {
          console.log('error faltan campos de el local storage')
        }
      }
    )
  }

  async handleGetStorageStart(id) {
    this.storage.get("user").then(
      user => {
        this.token = user.userToken;
        this.uid = user.userInfo.uid;

        if (user) {
          this.goInit(id);
        } else {
          console.log('error faltan campos de el local storage')
        }
      }
    )
  }

  async goInit(id) {
    this.typeNetwork = this.network.type;
    if (this.typeNetwork != 'none') {
      let data = {
        "id_service": id,
        "uid_technical": this.uid,
        "status": "2"
      };
      let request = this.request;
      let token = this.token
      const loading = await this.loadingController.create({
        message: 'Cargando...',
        duration: 2000,
        mode: 'ios'
      });
      await loading.present();
      await this._userProvider.handlePutService(data, token, request)
        .subscribe((res) => {
          this.updates = res;
          this.storage.set('status', 2);
          this.statusCamino = false;
          this.statusEmpieza = false;
          this.statusTermina = true;
          this.watch.unsubscribe();
          loading.dismiss();

        }, (err) => {
          loading.dismiss();
          this.alertMsj(
            "Ups! Ha ocurrido un error inesperado",
            "Por favor, intente de nuevo..."
          );
          console.error(err);
        });

    } else {
      this.alertMsj(
        "Error de conexión",
        "Verifica tu conexión a internet."
      );
    }
  }

  async empezarServicio() {
    const alert = await this.alertCtrl.create({
      header: 'Empezar el servicio',
      message: '¿Desea iniciar con el servicio?',
      mode: 'ios',
      buttons: [{
        text: 'Cancelar',
        role: 'cancel',
        cssClass: 'secondary',
        handler: (blah) => {

        }
      }, {
        text: 'Aceptar',
        handler: () => {
          this.goStart();
        }
      }]
    });

    await alert.present();
  }

  // FINALIZAR EL SERVICIO

  async goFinish() {
    this.storage.get("service").then(
      user => {
        this.id = user.listService.id_service;
        this.request = user.listService.id;

        if (this.id) {
          this.handleGetStorageFinish(this.id);
        } else {
          console.log('error faltan campos de el local storage')
        }
      }
    )
  }

  async handleGetStorageFinish(id) {
    this.storage.get("user").then(
      user => {
        this.token = user.userToken;
        this.uid = user.userInfo.uid;

        if (user) {
          this.goTerminado(id);
        } else {
          console.log('error faltan campos de el local storage')
        }
      }
    )
  }

  async goTerminado(id) {
    this.typeNetwork = this.network.type;
    if (this.typeNetwork != 'none') {
      let data = {
        "id_service": id,
        "uid_technical": this.uid,
        "status": "1"
      };
      let request = this.request;
      let token = this.token
      const loading = await this.loadingController.create({
        message: 'Cargando...',
        duration: 2000,
        mode: 'ios'
      });
      await loading.present();
      await this._userProvider.handlePutService(data, token, request)
        .subscribe((res) => {
          this.updates = res;
          this.storage.set('status', 1);
          this.router.navigate(['/techome']);
          this.alertMsj(
            "Servicio finalizado con éxito",
            "Se ha finalizado un servicio satisfactoriamente"
          );
          loading.dismiss();

        }, (err) => {
          loading.dismiss();
          this.alertMsj(
            "Ups! Ha ocurrido un error inesperado",
            "Por favor, intente de nuevo..."
          );
          console.error(err);
        });

    } else {
      this.alertMsj(
        "Error de conexión",
        "Verifica tu conexión a internet."
      );
    }
  }

  async servicioTerminado() {
    const alert = await this.alertCtrl.create({
      header: 'Finalizar el servicio',
      message: '¿Desea finalizar el servicio?',
      mode: 'ios',
      buttons: [{
        text: 'Cancelar',
        role: 'cancel',
        cssClass: 'secondary',
        handler: (blah) => {

        }
      }, {
        text: 'Aceptar',
        handler: () => {
          this.goFinish();
        }
      }]
    });

    await alert.present();
  }

  ////////////////////////////////////////////////////

  async alertMsj(header: string, message: string) {
    const alert = await this.alertCtrl.create({
      header: header,
      message: message,
      buttons: ['Ok'],
      mode: 'ios'
    });
    alert.present();
  }

  async getChat(){
    console.log(this.servicio.listService.id)
    const uid = this.servicio.listService.id.toString();
    await this._userProvider.getChat(uid).then(chat => {
      const chatInfo = chat.data();
      console.log(chatInfo);
      if(chatInfo) this.handleGoChat(uid);
      else this.errorChat();
    });
  }

  async handleGoChat(uid) {
    this.storage.get("service").then(
      user => {
        let id = user.listService.id;

        if (user) {
          this.goChat(uid);
        } else {
          console.log('error faltan campos de el local storage')
        }
      }
    )
  }

  goChat(id) {
    // Data que enviaremos
    let data = {
      "id": id,
      "idUs": this.idUsuario
    }
    console.log(data)
    let navigationExtras: NavigationExtras = {
      queryParams: {
        // dataCard: Nombre que indicamos que sera recibido en formularioinformacion
        // JSON.stringify(data): Recibe data y la convierte en un JSON
        dataCard: JSON.stringify(data)
      }
    };
    // Ruta de Page, NavigationExtras: Data a enviar
    this.router.navigate(['chat-tec'], navigationExtras);
  }

  errorChat(){
    this.alertMsj(
      "Ups! Ha ocurrido un error inesperado",
      "Por favor, intente de nuevo..."
    );
  }

  // SEND UBICATION

  sendMessage(id) {
    console.log(id);
    let watch = this.geolocation.watchPosition();
    this.watch = watch.subscribe((data) => {
      console.log(data.coords.latitude)
      console.log(data.coords.longitude)
      let messagesRef = firebase.database().ref("locations").child(id);
      messagesRef.push({
        lat: data.coords.latitude,
        lng: data.coords.longitude
      });
    });

  }

}