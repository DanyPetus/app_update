import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ServiceatendidoPageRoutingModule } from './serviceatendido-routing.module';

import { ServiceatendidoPage } from './serviceatendido.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ServiceatendidoPageRoutingModule
  ],
  declarations: [ServiceatendidoPage]
})
export class ServiceatendidoPageModule {}
