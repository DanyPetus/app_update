import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ServiceatendidoPage } from './serviceatendido.page';

const routes: Routes = [
  {
    path: '',
    component: ServiceatendidoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ServiceatendidoPageRoutingModule {}
