import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ChatTecPage } from './chat-tec.page';

const routes: Routes = [
  {
    path: '',
    component: ChatTecPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ChatTecPageRoutingModule {}
