import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ChatTecPageRoutingModule } from './chat-tec-routing.module';

import { ChatTecPage } from './chat-tec.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ChatTecPageRoutingModule
  ],
  declarations: [ChatTecPage]
})
export class ChatTecPageModule {}
