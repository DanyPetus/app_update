import {
  Component,
  OnInit
} from '@angular/core';
import {
  NavController,
  Platform,
  AlertController,
  LoadingController,
  MenuController
} from '@ionic/angular';
import {
  Router,
  NavigationExtras,
  ActivatedRoute
} from '@angular/router';
import {
  Network
} from '@ionic-native/network/ngx';
import {
  Storage
} from '@ionic/storage';

import {
  LoginService
} from '../../services/login/login.service';
import { EmailComposer } from '@ionic-native/email-composer/ngx';


@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  email: string;
  contrasena: string;
  login: any;
  typeNetwork: string;

  plt;

  constructor(
    private route: ActivatedRoute,
    private navCtrl: NavController, private router: Router,
    private platform: Platform,
    private network: Network,
    private alertCtrl: AlertController,
    private _loginProvider: LoginService,
    private storage: Storage,
    public loadingController: LoadingController,
    private menu: MenuController,
    private emailComposer: EmailComposer
  ) {
    if (this.platform.is('ios')) {
      this.plt = 'ios';
    } else {
      this.plt = 'android';
    }
  }

  Email(){
    // this.emailComposer.isAvailable().then((available: boolean) =>{
    //   if(available) {
        this.sendMail();
    //   }
    //  });
  }

  sendMail(){
    let email = {
      from: this.email,
      to: 'danyperalta28@yahoo.com.mx',
      cc: '',
      // bcc: ['john@doe.com', 'jane@doe.com'],
      attachments: [],
      subject: 'Solicitud de cambio de contraseña',
      body: 'Solicitud de cambio de contraseña al usuario de:' + this.email,
      isHtml: true
    };
    // Send a text message using default options
    this.emailComposer.open(email);
}

  ngOnInit() {
    this.openFirst();
  }

  openFirst() {
    this.menu.enable(false, 'first');
    this.menu.enable(false, 'second');
  }

  goBack() {
    this.navCtrl.back();
  }

  goToRegister() {
    this.router.navigate(['/registro']);
  }

  async goToHome() {
    this.typeNetwork = this.network.type;
    if (this.typeNetwork != 'none') {
      let data = {
        'email': this.email,
        'password': this.contrasena,
      };
      console.log(data);
      const loading = await this.loadingController.create({
        message: 'Cargando...',
        duration: 3000
      });
      await loading.present();
      await this._loginProvider.handleLog(data)
        .subscribe((res) => {
          this.login = res;
          console.log(this.login.userInfo)
          this.storage.set('user', this.login);
          if (this.login.userInfo.roleType === 3) {
            this.storage.set('user', this.login);
            this.storage.set('status', 1);
            this.router.navigate(['home']);
          } else if (this.login.userInfo.roleType === 4) {
            this.storage.set('user', this.login);
            this.router.navigate(['/techome']);
          }
          loading.dismiss();

        }, (err) => {
          this.alertMsj(
            "El usuario o la contraseña son incorrectos",
            "Por favor, intente de nuevo..."
          );
          console.error(err);
        });
    } else {
      this.alertMsj(
        "Error de conexión",
        "Verifica tu conexión a internet."
      );
    }
  }

  async alertMsj(header: string, message: string) {
    const alert = await this.alertCtrl.create({
      header: header,
      message: message,
      buttons: ['Ok'],
      mode: 'ios'
    });
    alert.present();
  }

  goToForgot() {
    this.router.navigate(['/forgot-password']);
  }

}