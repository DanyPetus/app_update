import {
  Component,
  OnInit,
  ViewChild,
  ElementRef
} from '@angular/core';
import {
  ActivatedRoute,
  Router,
  NavigationExtras
} from '@angular/router';
import {
  NavController,
  Platform
} from '@ionic/angular';
import {
  Geolocation
} from '@ionic-native/geolocation/ngx';
import {
  LoadingController
} from '@ionic/angular';
import {
  Subscription
} from 'rxjs';
import {
  filter
} from 'rxjs/operators';
import {
  Storage
} from '@ionic/storage';
import firebase from 'firebase';

declare var google;

@Component({
  selector: 'app-maptest',
  templateUrl: './maptest.page.html',
  styleUrls: ['./maptest.page.scss'],
})
export class MaptestPage implements OnInit {
  map: any;
  directionsService = new google.maps.DirectionsService();
  directionsDisplay = new google.maps.DirectionsRenderer();

  origin = {
    lat: 14.559381409029982,
    lng: -90.54956066581431
  };
  destination : any;

  origen: any;
  latDes: any;
  lngDes: any;

  destino: any;
  data: any;

  messages : any;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private navCtrl: NavController,
    private platform: Platform,
    private geolocation: Geolocation,
    public loadingController: LoadingController,
    private storage: Storage
  ) {}

  ngOnInit() {
    // Route recibe los parametros (se debe importar)
    this.route.queryParams.subscribe(params => {
      // Valida que exista params y params.dataCard que son los datos que recibiremos
      if (params && params.dataCard) {
        // Los parseamos nuevamente a objeto y podemos acceder a sus propiedades
        this.data = JSON.parse(params.dataCard);
        console.log(this.data);
      }
    });
    this.getMessages();
  }

  async loadMap() {
    const loading = await this.loadingController.create({
      message: 'Cargando...',
      duration: 2000
    });
    await loading.present();
    console.log("si")
    this.geolocation.getCurrentPosition().then((resp) => {
      console.log("no")
      this.origen = {
        lat: resp.coords.latitude,
        lng: resp.coords.longitude
      }
      console.log(this.origen)
      const mapEle: HTMLElement = document.getElementById('map');
      this.map = new google.maps.Map(mapEle, {
        center: this.origen,
        zoom: 17,
        // styles: [{
        //     elementType: 'geometry',
        //     stylers: [{
        //       color: '#242f3e'
        //     }]
        //   },
        //   {
        //     elementType: 'labels.text.stroke',
        //     stylers: [{
        //       color: '#242f3e'
        //     }]
        //   },
        //   {
        //     elementType: 'labels.text.fill',
        //     stylers: [{
        //       color: '#746855'
        //     }]
        //   },
        //   {
        //     featureType: 'administrative.locality',
        //     elementType: 'labels.text.fill',
        //     stylers: [{
        //       color: '#d59563'
        //     }]
        //   },
        //   {
        //     featureType: 'poi',
        //     elementType: 'labels.text.fill',
        //     stylers: [{
        //       color: '#d59563'
        //     }]
        //   },
        //   {
        //     featureType: 'poi.park',
        //     elementType: 'geometry',
        //     stylers: [{
        //       color: '#263c3f'
        //     }]
        //   },
        //   {
        //     featureType: 'poi.park',
        //     elementType: 'labels.text.fill',
        //     stylers: [{
        //       color: '#6b9a76'
        //     }]
        //   },
        //   {
        //     featureType: 'road',
        //     elementType: 'geometry',
        //     stylers: [{
        //       color: '#38414e'
        //     }]
        //   },
        //   {
        //     featureType: 'road',
        //     elementType: 'geometry.stroke',
        //     stylers: [{
        //       color: '#212a37'
        //     }]
        //   },
        //   {
        //     featureType: 'road',
        //     elementType: 'labels.text.fill',
        //     stylers: [{
        //       color: '#9ca5b3'
        //     }]
        //   },
        //   {
        //     featureType: 'road.highway',
        //     elementType: 'geometry',
        //     stylers: [{
        //       color: '#746855'
        //     }]
        //   },
        //   {
        //     featureType: 'road.highway',
        //     elementType: 'geometry.stroke',
        //     stylers: [{
        //       color: '#1f2835'
        //     }]
        //   },
        //   {
        //     featureType: 'road.highway',
        //     elementType: 'labels.text.fill',
        //     stylers: [{
        //       color: '#f3d19c'
        //     }]
        //   },
        //   {
        //     featureType: 'transit',
        //     elementType: 'geometry',
        //     stylers: [{
        //       color: '#2f3948'
        //     }]
        //   },
        //   {
        //     featureType: 'transit.station',
        //     elementType: 'labels.text.fill',
        //     stylers: [{
        //       color: '#d59563'
        //     }]
        //   },
        //   {
        //     featureType: 'water',
        //     elementType: 'geometry',
        //     stylers: [{
        //       color: '#17263c'
        //     }]
        //   },
        //   {
        //     featureType: 'water',
        //     elementType: 'labels.text.fill',
        //     stylers: [{
        //       color: '#515c6d'
        //     }]
        //   },
        //   {
        //     featureType: 'water',
        //     elementType: 'labels.text.stroke',
        //     stylers: [{
        //       color: '#17263c'
        //     }]
        //   }
        // ]
      });

      this.directionsDisplay.setMap(this.map);

      google.maps.event.addListenerOnce(this.map, 'idle', () => {
        loading.dismiss();
        mapEle.classList.add('show-map');
        this.calculateRoute();
      });
    }).catch((error) => {
      console.log('Error getting location', error);
    });
  }


  private calculateRoute() {
    this.directionsService.route({
      origin: this.origen,
      destination: this.destination,
      travelMode: google.maps.TravelMode.DRIVING,
    }, (response, status) => {
      if (status === google.maps.DirectionsStatus.OK) {
        this.directionsDisplay.setDirections(response);
      } else {
        alert('could not display directions due to: ' + status)
      }
    });
  }

  async getMessages() {
    // this.data.listService.id
    let messagesRef = firebase.database().ref("locations").child(this.data.listService.id);
    messagesRef.on("value", (snap) => {
      let data = snap.val();
      this.messages = [];
      for (var key in data) {
        this.messages.push(data[key]);
      }
      console.log(this.messages);
      this.messages.forEach(element => {
        this.destination = element;
      });
      this.loadMap();
      console.log(this.destination)
    });
  }


}