import {
  Component,
  OnInit
} from '@angular/core';
import {
  NavController,
  Platform,
  AlertController,
  LoadingController,
  MenuController,
  ModalController
} from '@ionic/angular';
import {
  Network
} from '@ionic-native/network/ngx';
import {
  Storage
} from '@ionic/storage';
import {
  ActivatedRoute,
  Router,
  NavigationExtras
} from '@angular/router';
import {
  ModalValoracionPage
} from '../modal-valoracion/modal-valoracion.page';
import {
  UserService
} from '../../services/user/user.service';

import {
  FCM
} from '@ionic-native/fcm/ngx';
import {
  ToastController
} from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  typeNetwork: string;
  servicios: any;
  token: any;
  uid: string;
  procesos: any;
  mostrar: boolean = false;

  listadosFilter: any;
  tecnicos: any;
  tokenFCM: string;
  routerFCM: any;
  tokens: any;
  tokensService: any;

  serviciosListados: any = [];
  enprocesos0: any;
  enprocesos4: any;
  enprocesos3: any;
  enprocesos2: any;
  terminados: any;

  data: any;
  constructor(
    private navCtrl: NavController, private router: Router,
    private platform: Platform,
    private modalCtrl: ModalController,
    private network: Network,
    private alertCtrl: AlertController,
    private _userProvider: UserService,
    private storage: Storage,
    public loadingController: LoadingController,
    private menu: MenuController,
    private fcm: FCM,
    public toastController: ToastController,
    private route: ActivatedRoute,
  ) {
  }

  ngOnInit() {
    this.platform.ready().then(() => {
    this.storage.get("status").then(
      status => {
        if (status === 0) {
          this.getServicesOffline();
        } else {
          this.fcm.getToken().then(token => {
            this.tokenFCM = token;
            console.log("ESTES ES EL TOKEN", this.token);
          });
          this.fcm.requestPushPermissionIOS({
            ios9Support: {
              timeout: 10, // How long it will wait for a decision from the user before returning `false`
              interval: 0.3 // How long between each permission verification
            }
          })
          this.fcm.onNotification().subscribe(data => {
            console.log("DATA" + JSON.stringify(data));
            if (data.wasTapped) {
              this.router.navigate(['/home', data]);
              if (data.title === "Servicio completado") {
                this.terminadosPunt();
              } else {

              }
            } else {
              this.presentToastWithOptions();
              console.log("DATA" + JSON.stringify(data));
              if (data.title === "Servicio completado") {
                this.terminadosPunt();
              } else {

              }
            };
          });
          this.fcm.onTokenRefresh().subscribe(token => {
            this.tokenFCM = token;
            console.log(this.token);
          });
        }
      })
    });
  }

  ionViewWillEnter() {
    this.platform.ready().then(() => {
    this.storage.get("status").then(
      status => {
        if (status === 0) {
          this.getServicesOffline();
        } else {
          this.serviciosListados = [];
          this.openFirst();
          this.handleGetStorageUser();
        }
      });
    });
  }

  async checkValoration(id, id_request, servicio, tecnicoName, tecnicoLastName) {
    const loading = await this.loadingController.create({
      message: 'Cargando...',
      duration: 2000,
      mode: 'ios'
    });
    await loading.present();
    const modal = await this.modalCtrl.create({
      component: ModalValoracionPage,
      componentProps: {
        'id': id,
        'id_request': id_request,
        'servicio': servicio,
        'tecnicoName': tecnicoName,
        'tecnicoLastName': tecnicoLastName
      },
      cssClass: 'modalorder',
    });
    await modal.present();
    modal.onDidDismiss()
      .then((res) => {
        console.log("nice")
      })
    loading.dismiss();
  }

  async handleGetUID() {
    this.storage.get("user").then(
      user => {
        this.token = user.userToken;
        this.uid = user.userInfo.uid;

        if (user) {
          this.terminadosPunt();
        } else {
          console.log('error faltan campos de el local storage')
        }
      }
    )
  }

  async terminadosPunt() {
    this.typeNetwork = this.network.type;
    if (this.typeNetwork != 'none') {
      let data = {
        "uid_user": this.uid,
        "status": 1,
        "punctuation": 0
      };
      let token = this.token
      // console.log(data, token);
      const loading = await this.loadingController.create({
        message: 'Cargando...',
        duration: 3000
      });
      await loading.present();
      await this._userProvider.handleGetServicioss(data, token)
        .subscribe((res) => {
          this.terminados = res;
          console.log(this.terminados);
          let id = this.terminados[0].listService.id;
          let id_request = this.terminados[0].listService.id_service;
          let servicio = this.terminados[0].listService.service.name;
          let tecnicoName = this.terminados[0].users.technical.firstName;
          let tecnicoLastName = this.terminados[0].users.technical.secondName;
          this.checkValoration(id, id_request, servicio, tecnicoName, tecnicoLastName);
          loading.dismiss();

        }, (err) => {
          this.alertMsj(
            "Ups! Ha ocurrido un error inesperado",
            "Por favor, intente de nuevo..."
          );
          loading.dismiss();
          console.error(err);
        });

    } else {
      this.alertMsj(
        "Error de conexión",
        "Verifica tu conexión a internet."
      );
    }
  }

  customPopoverOptions: any = {
    header: 'Servicios',
    subHeader: 'Selecciona un servicio',
  };

  buscar(ev) {
    let val = ev.detail.value;

    if (val && val.trim() !== '') {
      this.listadosFilter = this.servicios.filter(term =>
        term.name.toLowerCase().indexOf(val.toLowerCase().trim()) > -1)
    } else {
      this.listadosFilter = this.servicios;
    }
  }

  async sendToken() {
    if (this.tokenFCM === undefined){
      this.tokenFCM = '12345678'
    } 
    this.typeNetwork = this.network.type;
    if (this.typeNetwork != 'none') {
      let data = {
        "uid": this.uid,
        "token": this.tokenFCM
      }
      let token = this.token
      console.log(token)
      console.log(data)
      const loading = await this.loadingController.create({
        message: 'Cargando...',
        duration: 3000
      });
      await loading.present();
      await this._userProvider.handleSendToken(data, token)
        .subscribe((res) => {
          this.tokens = res;
          console.log('TOKEN ' + this.tokens);
          loading.dismiss();

        }, (err) => {
          // this.alertMsj(
          //   "Ups! Ha ocurrido un error inesperado de notificaciones",
          //   "Por favor, intente de nuevo..."
          // );
          loading.dismiss();
          console.error(err);
        });
    } else {
      this.alertMsj(
        "Error de conexión",
        "Verifica tu conexión a internet."
      );
    }
  }

  async presentToastWithOptions() {
    const toast = await this.toastController.create({
      header: 'Notificacion entrante',
      message: 'Se ha actualizado el status de su servicio',
      position: 'top',
      buttons: [{
        side: 'start',
        icon: 'star',
        text: 'OK',
        handler: () => {
          toast.dismiss();
        }
      }, {
        text: 'Hecho',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
          toast.dismiss();
        }
      }]
    });
    toast.present();
  }

  openFirst() {
    this.menu.enable(true, 'first');
    this.menu.enable(false, 'second');
  }

  async handleGetStorageUser() {
    this.storage.get("user").then(
      user => {
        this.token = user.userToken;
        this.uid = user.userInfo.uid;

        if (user) {
          this.getServicesOnline();
          this.getProcesos();
          this.sendToken();
          this.getenprocesos5();
          this.getenprocesos4();
          this.getenprocesos3();
          this.getenprocesos2();

          // this.terminadosPunt();

        } else {
          console.log('error faltan campos de el local storage')
        }
      }
    )
  }

  async getServicesOffline() {
    this.typeNetwork = this.network.type;
    if (this.typeNetwork != 'none') {
      let data = {
        "status": 1
      };
      const loading = await this.loadingController.create({
        message: 'Cargando...',
        duration: 3000
      });
      await loading.present();
      await this._userProvider.handleGetServiceOffline(data)
        .subscribe((res) => {
          this.servicios = res;
          this.listadosFilter = this.servicios;
          console.log(this.servicios);
          loading.dismiss();

        }, (err) => {
          this.alertMsj(
            "Ups! Ha ocurrido un error inesperado",
            "Por favor, intente de nuevo..."
          );
          loading.dismiss();
          console.error(err);
        });
    } else {
      this.alertMsj(
        "Error de conexión",
        "Verifica tu conexión a internet."
      );
    }
  }

  async getServicesOnline() {
    this.typeNetwork = this.network.type;
    if (this.typeNetwork != 'none') {
      let data = {
        "status": 1
      };
      let token = this.token
      console.log(token);
      const loading = await this.loadingController.create({
        message: 'Cargando...',
        duration: 3000
      });
      await loading.present();
      await this._userProvider.handleGetServiceOnline(data, token)
        .subscribe((res) => {
          this.servicios = res;
          this.listadosFilter = this.servicios;
          console.log(this.servicios);
          loading.dismiss();

        }, (err) => {
          this.alertMsj(
            "Ups! Ha ocurrido un error inesperado",
            "Por favor, intente de nuevo..."
          );
          loading.dismiss();
          console.error(err);
        });
    } else {
      this.alertMsj(
        "Error de conexión",
        "Verifica tu conexión a internet."
      );
    }
  }

  async getProcesos() {
    this.typeNetwork = this.network.type;
    if (this.typeNetwork != 'none') {
      let data = {
        "uid_user": this.uid,
        "status": "3",
      };
      console.log("DATA", data);
      let token = this.token
      await this._userProvider.handleGetServicioss(data, token)
        .subscribe((res) => {
          this.procesos = res;
          if (this.procesos) {
            this.mostrar = true
          }
          console.log(this.procesos);

        }, (err) => {
          this.alertMsj(
            "Ups! Ha ocurrido un error inesperado",
            "Por favor, intente de nuevo..."
          );
          console.error(err);
        });

    } else {
      this.alertMsj(
        "Error de conexión",
        "Verifica tu conexión a internet."
      );
    }
  }

  async handleGetStorageTecnico() {
    this.storage.get("tecnico").then(
      user => {
        this.tecnicos = user;
        // console.log(this.tecnicos)

        if (user) {
          this.goTrack(this.tecnicos);
        } else {
          console.log('error faltan campos de el local storage')
        }
      }
    )
  }

  goTrack(tecnico) {
    // Data que enviaremos
    let data = {
      "tecnico": tecnico
    }
    let navigationExtras: NavigationExtras = {
      queryParams: {
        // dataCard: Nombre que indicamos que sera recibido en formularioinformacion
        // JSON.stringify(data): Recibe data y la convierte en un JSON
        dataCard: JSON.stringify(data)
      }
    };
    // Ruta de Page, NavigationExtras: Data a enviar
    this.router.navigate(['track'], navigationExtras);
  }

  async alertMsj(header: string, message: string) {
    const alert = await this.alertCtrl.create({
      header: header,
      message: message,
      buttons: ['Ok'],
      mode: 'ios'
    });
    alert.present();
  }

  // IR AL SERVICIO

  goService(servicio) {
    // Data que enviaremos
    let data = servicio
    let navigationExtras: NavigationExtras = {
      queryParams: {
        // dataCard: Nombre que indicamos que sera recibido en formularioinformacion
        // JSON.stringify(data): Recibe data y la convierte en un JSON
        dataCard: JSON.stringify(data)
      }
    };
    // Ruta de Page, NavigationExtras: Data a enviar
    this.router.navigate(['calculos'], navigationExtras);
  }

  async getenprocesos5() {
    this.typeNetwork = this.network.type;
    if (this.typeNetwork != 'none') {
      let data = {
        "uid_user": this.uid,
        "status": 5,
      };
      let token = this.token
      // console.log(data, token);
      const loading = await this.loadingController.create({
        message: 'Cargando...',
        duration: 3000
      });
      await loading.present();
      await this._userProvider.handleGetServicioss(data, token)
        .subscribe((res) => {
          this.enprocesos0 = res;
          this.enprocesos0.forEach(element => {
            this.serviciosListados.push(element)
          });
          console.log(this.enprocesos0);
          loading.dismiss();

        }, (err) => {
          this.alertMsj(
            "Ups! Ha ocurrido un error inesperado",
            "Por favor, intente de nuevo..."
          );
          loading.dismiss();
          console.error(err);
        });

    } else {
      this.alertMsj(
        "Error de conexión",
        "Verifica tu conexión a internet."
      );
    }
  }

  async getenprocesos4() {
    this.typeNetwork = this.network.type;
    if (this.typeNetwork != 'none') {
      let data = {
        "uid_user": this.uid,
        "status": 4,
      };
      let token = this.token
      // console.log(data, token);
      const loading = await this.loadingController.create({
        message: 'Cargando...',
        duration: 3000
      });
      await loading.present();
      await this._userProvider.handleGetServicioss(data, token)
        .subscribe((res) => {
          this.enprocesos4 = res;
          this.enprocesos4.forEach(element => {
            this.serviciosListados.push(element)
          });
          console.log(this.enprocesos4);
          loading.dismiss();

        }, (err) => {
          this.alertMsj(
            "Ups! Ha ocurrido un error inesperado",
            "Por favor, intente de nuevo..."
          );
          loading.dismiss();
          console.error(err);
        });

    } else {
      this.alertMsj(
        "Error de conexión",
        "Verifica tu conexión a internet."
      );
    }
  }

  async getenprocesos3() {
    this.typeNetwork = this.network.type;
    if (this.typeNetwork != 'none') {
      let data = {
        "uid_user": this.uid,
        "status": 3,
      };
      let token = this.token
      // console.log(data, token);
      const loading = await this.loadingController.create({
        message: 'Cargando...',
        duration: 3000
      });
      await loading.present();
      await this._userProvider.handleGetServicioss(data, token)
        .subscribe((res) => {
          this.enprocesos3 = res;
          this.enprocesos3.forEach(element => {
            this.serviciosListados.push(element)
          });
          console.log(this.enprocesos3);
          loading.dismiss();

        }, (err) => {
          this.alertMsj(
            "Ups! Ha ocurrido un error inesperado",
            "Por favor, intente de nuevo..."
          );
          loading.dismiss();
          console.error(err);
        });

    } else {
      this.alertMsj(
        "Error de conexión",
        "Verifica tu conexión a internet."
      );
    }
  }

  async getenprocesos2() {
    this.typeNetwork = this.network.type;
    if (this.typeNetwork != 'none') {
      let data = {
        "uid_user": this.uid,
        "status": 2,
      };
      let token = this.token
      // console.log(data, token);
      const loading = await this.loadingController.create({
        message: 'Cargando...',
        duration: 3000
      });
      await loading.present();
      await this._userProvider.handleGetServicioss(data, token)
        .subscribe((res) => {
          this.enprocesos2 = res;
          this.enprocesos2.forEach(element => {
            this.serviciosListados.push(element)
          });
          console.log(this.enprocesos2);
          console.log(this.serviciosListados);
          loading.dismiss();

        }, (err) => {
          this.alertMsj(
            "Ups! Ha ocurrido un error inesperado",
            "Por favor, intente de nuevo..."
          );
          loading.dismiss();
          console.error(err);
        });

    } else {
      this.alertMsj(
        "Error de conexión",
        "Verifica tu conexión a internet."
      );
    }
  }

  goSeeService(ev) {
    let evento = JSON.parse(ev.detail.value);
    // Data que enviaremos
    let data = evento
    let navigationExtras: NavigationExtras = {
      queryParams: {
        // dataCard: Nombre que indicamos que sera recibido en formularioinformacion
        // JSON.stringify(data): Recibe data y la convierte en un JSON
        dataCard: JSON.stringify(data)
      }
    };
    // Ruta de Page, NavigationExtras: Data a enviar
    this.router.navigate(['track'], navigationExtras);
  }

}