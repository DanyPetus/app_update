import { Component, OnInit, Input } from '@angular/core';
import {
  NavController,
  Platform,
  AlertController,
  LoadingController,
  MenuController,
  ModalController
} from '@ionic/angular';
import {
  ActivatedRoute,
  Router,
  NavigationExtras
} from '@angular/router';
import {
  Network
} from '@ionic-native/network/ngx';
import {
  Storage
} from '@ionic/storage';
import {
  UserService
} from '../../services/user/user.service';

@Component({
  selector: 'app-modal-valoracion',
  templateUrl: './modal-valoracion.page.html',
  styleUrls: ['./modal-valoracion.page.scss'],
})
export class ModalValoracionPage implements OnInit {
  typeNetwork: string;
  token : any;
  terminados : any;
  uid : any;
  puntuacion : number = 0;

  mostrar0 : boolean = true;
  mostrar1 : boolean = true;
  mostrar2 : boolean = true;
  mostrar3 : boolean = true;
  mostrar4 : boolean = true;

  @Input() id: any;
  @Input() id_request: any;
  @Input() servicio: any;
  @Input() tecnicoName: any;
  @Input() tecnicoLastName: any;


  constructor(
    private alertCtrl: AlertController,
    private navCtrl: NavController, private router: Router,
    private network: Network,
    private storage: Storage,
    public loadingController: LoadingController,
    private _userProvider: UserService,
    private modalCtrl: ModalController,
  ) { }

  ngOnInit() {
    // this.handleGetStorageFinish();
    console.log(this.id)
    console.log(this.id_request)
    console.log(this.servicio)
    console.log(this.tecnicoName)
    console.log(this.tecnicoLastName)
  }

  check(valor){
    console.log(valor)
    if (valor === 1){
      this.puntuacion = 1;
      this.mostrar0 = false;
      this.mostrar1 = true;
      this.mostrar2 = true;
      this.mostrar3 = true;
      this.mostrar4 = true;
    } else if (valor === 2){
      this.puntuacion = 2;
      this.mostrar0 = false;
      this.mostrar1 = false;
      this.mostrar2 = true;
      this.mostrar3 = true;
      this.mostrar4 = true;
    } else if (valor === 3){
      this.puntuacion = 3;
      this.mostrar0 = false;
      this.mostrar1 = false;
      this.mostrar2 = false;
      this.mostrar3 = true;
      this.mostrar4 = true;
    } else if (valor === 4){
      this.puntuacion = 4;
      this.mostrar0 = false;
      this.mostrar1 = false;
      this.mostrar2 = false;
      this.mostrar3 = false;
      this.mostrar4 = true;
    } else if (valor === 5){
      this.puntuacion = 5;
      this.mostrar0 = false;
      this.mostrar1 = false;
      this.mostrar2 = false;
      this.mostrar3 = false;
      this.mostrar4 = false;
    } else if (valor === 6){
      this.puntuacion = 1;
      this.mostrar0 = false;
      this.mostrar1 = true;
      this.mostrar2 = true;
      this.mostrar3 = true;
      this.mostrar4 = true;
    } else if (valor === 7){
      this.puntuacion = 2;
      this.mostrar0 = false;
      this.mostrar1 = false;
      this.mostrar2 = true;
      this.mostrar3 = true;
      this.mostrar4 = true;
    } else if (valor === 8){
      this.puntuacion = 3;
      this.mostrar0 = false;
      this.mostrar1 = false;
      this.mostrar2 = false;
      this.mostrar3 = true;
      this.mostrar4 = true;
    } else if (valor === 9){
      this.puntuacion = 4;
      this.mostrar0 = false;
      this.mostrar1 = false;
      this.mostrar2 = false;
      this.mostrar3 = false;
      this.mostrar4 = true;
    } else if (valor === 10){
      this.puntuacion = 5;
      this.mostrar0 = false;
      this.mostrar1 = false;
      this.mostrar2 = false;
      this.mostrar3 = false;
      this.mostrar4 = false;
    } 
  }

  async handleGetStorageFinish() {
    this.storage.get("user").then(
      user => {
        this.token = user.userToken;
        this.uid = user.userInfo.uid;

        if (user) {
          this.goTerminado();
        } else {
          console.log('error faltan campos de el local storage')
        }
      }
    )
  }

  async goTerminado() {
    this.typeNetwork = this.network.type;
    if (this.typeNetwork != 'none') {
      let data = {
        "id_service": this.id_request,
        "uid_user": this.uid,
        "status": "1",
        "punctuation": this.puntuacion
      };
      let request = this.id;
      let token = this.token
      console.log(data);
      console.log(request);
      console.log(token);

      const loading = await this.loadingController.create({
        message: 'Cargando...',
        duration: 2000,
        mode: 'ios'
      });
      await loading.present();
      await this._userProvider.handlePutService(data, token, request)
        .subscribe((res) => {
          this.terminados = res;
          this.router.navigate(['/home']);
          this.alertMsj(
            "Gracias por calificar nuestro servicio",
            "Se ha finalizado el servicio satisfactoriamente"
          );
          this.modalCtrl.dismiss();
          loading.dismiss();

        }, (err) => {
          loading.dismiss();
          this.alertMsj(
            "Ups! Ha ocurrido un error inesperado",
            "Por favor, intente de nuevo..."
          );
          console.error(err);
        });

    } else {
      this.alertMsj(
        "Error de conexión",
        "Verifica tu conexión a internet."
      );
    }
  }

  async alertMsj(header: string, message: string) {
    const alert = await this.alertCtrl.create({
      header: header,
      message: message,
      buttons: ['Ok'],
      mode: 'ios'
    });
    alert.present();
  }

}
