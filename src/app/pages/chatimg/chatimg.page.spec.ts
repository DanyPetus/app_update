import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ChatimgPage } from './chatimg.page';

describe('ChatimgPage', () => {
  let component: ChatimgPage;
  let fixture: ComponentFixture<ChatimgPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChatimgPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ChatimgPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
