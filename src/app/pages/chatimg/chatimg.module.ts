import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ChatimgPageRoutingModule } from './chatimg-routing.module';

import { ChatimgPage } from './chatimg.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ChatimgPageRoutingModule
  ],
  declarations: [ChatimgPage]
})
export class ChatimgPageModule {}
