import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ChatimgPage } from './chatimg.page';

const routes: Routes = [
  {
    path: '',
    component: ChatimgPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ChatimgPageRoutingModule {}
