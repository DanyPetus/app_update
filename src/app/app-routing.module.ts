import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./pages/home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'intro',
    pathMatch: 'full'
  },
  {
    path: 'intro',
    loadChildren: () => import('./pages/intro/intro.module').then( m => m.IntroPageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./pages/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'forgot-password',
    loadChildren: () => import('./pages/forgot-password/forgot-password.module').then( m => m.ForgotPasswordPageModule)
  },
  {
    path: 'registro',
    loadChildren: () => import('./pages/registro/registro.module').then( m => m.RegistroPageModule)
  },
  {
    path: 'calculos',
    loadChildren: () => import('./pages/calculos/calculos.module').then( m => m.CalculosPageModule)
  },
  {
    path: 'checkout',
    loadChildren: () => import('./pages/checkout/checkout.module').then( m => m.CheckoutPageModule)
  },
  {
    path: 'ficha',
    loadChildren: () => import('./pages/ficha/ficha.module').then( m => m.FichaPageModule)
  },
  {
    path: 'modal-order',
    loadChildren: () => import('./pages/modal-order/modal-order.module').then( m => m.ModalOrderPageModule)
  },
  {
    path: 'pago',
    loadChildren: () => import('./pages/pago/pago.module').then( m => m.PagoPageModule)
  },
  {
    path: 'tecnicos',
    loadChildren: () => import('./pages/tecnicos/tecnicos.module').then( m => m.TecnicosPageModule)
  },
  {
    path: 'track',
    loadChildren: () => import('./pages/track/track.module').then( m => m.TrackPageModule)
  },
  {
    path: 'track-map',
    loadChildren: () => import('./pages/track-map/track-map.module').then( m => m.TrackMapPageModule)
  },
  {
    path: 'historial',
    loadChildren: () => import('./pages/historial/historial.module').then( m => m.HistorialPageModule)
  },
  {
    path: 'techome',
    loadChildren: () => import('./pages/tec/techome/techome.module').then( m => m.TechomePageModule)
  },
  {
    path: 'pedidostec',
    loadChildren: () => import('./pages/tec/pedidostec/pedidostec.module').then( m => m.PedidostecPageModule)
  },
  {
    path: 'serviceatendido',
    loadChildren: () => import('./pages/tec/serviceatendido/serviceatendido.module').then( m => m.ServiceatendidoPageModule)
  },
  {
    path: 'terminos',
    loadChildren: () => import('./pages/terminos/terminos.module').then( m => m.TerminosPageModule)
  },
  {
    path: 'maptest',
    loadChildren: () => import('./pages/maptest/maptest.module').then( m => m.MaptestPageModule)
  },
  {
    path: 'chat',
    loadChildren: () => import('./pages/chat/chat.module').then( m => m.ChatPageModule)
  },
  {
    path: 'chat-tec',
    loadChildren: () => import('./pages/tec/chat-tec/chat-tec.module').then( m => m.ChatTecPageModule)
  },
  {
    path: 'modal-valoracion',
    loadChildren: () => import('./pages/modal-valoracion/modal-valoracion.module').then( m => m.ModalValoracionPageModule)
  },
  {
    path: 'mapadirection',
    loadChildren: () => import('./pages/mapadirection/mapadirection.module').then( m => m.MapadirectionPageModule)
  },
  {
    path: 'chatimg',
    loadChildren: () => import('./pages/chatimg/chatimg.module').then( m => m.ChatimgPageModule)
  },

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules, relativeLinkResolution: 'legacy' })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
